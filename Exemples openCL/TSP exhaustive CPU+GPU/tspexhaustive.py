from __future__ import print_function
import pyopencl as cl
import pyopencl.tools
import pyopencl.array
import math
import itertools
from timeit import Timer
import time
from random import randint
import sys
import numpy as np
import math

class Point():
    def __init__(self, field1, field2):
        self.x = field1
        self.y = field2

    def __str__(self):
        return "x:"+str(self.x)+" y:"+str(self.y)

    def dist(self , p):
        return math.hypot(p.x - self.x , p.y - self.y)

# some utilities
def randomSeed(n , min , max):
    l = []
    for i in range (n):
        l.append(Point(randint(min,max) , randint(min,max)))
    return l

def bruteForce(l):
    min = float('Inf')
    tmp = 0
    counter = 0
    for i in itertools.permutations(l):
        for j in range(0,len(i)-1):
            #if (tmp > min):
            #    break
            counter+=1
            tmp+= i[j].dist(i[j+1])
#            print (str(i[j])+" + "+str(i[j+1]) , end=" ")
#            print("dist = %s" % np.around(tmp,0),end=" ")
#            print (tmp)
#        print(tmp)
        if ( tmp < min) : min = tmp
        tmp = 0
#        print ("")
    print ("cpu min : "+str(min))
    return min

def printStuff(l):
    r = ""
    for i in l:
        r+=str(i)+" | "
    print (r)

def get_XY_List(l):
    a = []
    b = []
    for i in range(len(l)):
        a.append(l[i].x)
        b.append(l[i].y)
    return a , b



# gpu code (python)
def gpuExhaustive(l):
    start = time.time()
    platforms = cl.get_platforms()
    ctx = cl.Context(dev_type=cl.device_type.ALL , properties=[(cl.context_properties.PLATFORM , platforms[1])])
    queue = cl.CommandQueue(ctx)
    x , y = get_XY_List(l)
    apx = np.array(x)
    apy = np.array(y)
    arx = cl.array.to_device(queue , apx)
    ary = cl.array.to_device(queue , apy)

    prg = cl.Program(ctx, "#define SIZE "+str(len(l))+open("exhaustiveKernel.c").read()).build()
    mf = cl.mem_flags

    output_size = 384
    it_number = math.factorial(len(l))

    r = np.empty(output_size , np.float32)
    for i in range(len(r)):
        r[i] = -1
    r = cl.array.to_device(queue , r)

    print("cpu to gpu time : "+str(time.time() - start))
    t = Timer(lambda: prg.gpuTSP(queue, cl.array.to_device(queue , np.empty(it_number , np.float32)).shape , None, r.data , arx.data , ary.data)).timeit(number=1)
    print ("gpu computation time : "+str(t))

    start = time.time()
    result = np.empty(output_size , np.float32)
    cl.enqueue_copy(queue , result , r.data).wait()
    print("gpu to cpu time : "+str(time.time() - start))
    print("gpu found : "+str(min(i for i in result if i > 0)))
#    print(result)
 
    

if __name__ == "__main__" :
    l = randomSeed(5 , 0 , 100)
    printStuff(l)
    print("cpu test with %s points" % len(l))
    t = Timer(lambda: bruteForce(l)).timeit(number=1)
    print("cpu time :"+str(t),end="\n\n")
    print("gpu test with %s points" % len(l))
    test = Timer (lambda :( gpuExhaustive(l))).timeit(number=1)
    print ("gpu total time : "+str(test))
