
#define OUTPUT_SIZE 384
#define CORE 384

unsigned long factorial(unsigned long f)
{
    if ( f == 0 ) 
        return 1;
    return(f * factorial(f - 1));
}

float dist ( float ax , float ay , float bx , float by)
{
  return sqrt((ax - bx)*(ax - bx) + (ay - by)*(ay - by));
}

float totalDist (int ax[] , int ay[] ){
  int i;
  float r = 0;
  for (i = 0 ; i <= SIZE-2 ; i++){
    r += dist(ax[i] , ay[i] , ax[i+1] , ay[i+1]);
  }
  return r;
}

int* nthPermutation(int k , int a[]){
  int P[SIZE];
  int fact;
  int index;
  int i,j = 0;
  int x;
  int n = SIZE;
  int temp[SIZE];
  while (n){
    fact = factorial(n-1);
    index = floor((float)k/fact);
    x = a[index];
    k = k % fact;
    P[j] = x;
 
    for (i = 0 ; i < index ; i++){
      temp[i] = a[i];
    }
 
    for (i = 0 ; i < n-index-1 ; i++){
      a[i] = a[index+i+1];
 
    }
 
    for (i = 0 ; i < index ; i++){
      a[n-index+i-1] = temp[i];
    }
    j++;
    --n;
  }
  
  return P;
}

__kernel void gpuTSP(
		  __global  float *test, __global int *x , __global int *y)
{
  int id = get_global_id(0);
  int ax [SIZE];
  int ay [SIZE];
  int i,j;
  for (i = 0 ; i <= SIZE ; i++){
    ax[i] = x[i];
  }
  for (i = 0 ; i <= SIZE ; i++) {
    ay[i] = y[i];
  }
    int *pax = nthPermutation(id , ax);
    for (i = 0 ; i <= SIZE ; i++){
      ax[i] = pax[i];
    }
  
    int *pay = nthPermutation(id , ay);
    for (i = 0 ; i <= SIZE ; i++){
      ay[i] = pay[i];
    }
    float result = totalDist(ax , ay);
    if (test[id%OUTPUT_SIZE] == -1 || result < test[id%OUTPUT_SIZE])
      {
	test[id%OUTPUT_SIZE] = result;
      }
  }
