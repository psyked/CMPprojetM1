import pyopencl as cl
from pyopencl import array
import numpy
from random import randrange
from math import sqrt
from copy import deepcopy
import matplotlib.pyplot as plot
import time


def generate_random_map(nbpoints, maxx, maxy, minx=0, miny=0):
    if (maxx - minx) * (maxy - miny) < nbpoints: return None
    map = []
    weights = []
    # create map
    for i in range(0, nbpoints):
        point = None
        while point is None or point in map:
            point = (randrange(minx, maxx), randrange(miny, maxy))
        map.append(point)
    # compute weight map
    for i in range(0, nbpoints):
        line = []
        for j in range(0, nbpoints):
            if i == j:
                line.append(None)
            else:
                line.append(sqrt((map[i][0] - map[j][0]) ** 2 + (map[i][1] - map[j][1]) ** 2))
        weights.append(line)
    return map, weights


def generated_random_weight(wmin, wmax, names):
    weight = []
    for i in range(0, len(names)):
        line = []
        for j in range(0, len(names)):
            if i == j:
                line.append(None)
            else:
                line.append(randrange(wmin, wmax))
        weight.append(line)
    return weight


def generate_random_hamiltonian_track(weights, size=None):
    if size is None: size = len(weights)
    track = []
    for i in range(0, size): track.append(0)
    unused = []
    for i in range(1, size): unused.append(i)
    previous = 0
    for i in range(0, size-1):
        index = randrange(0, len(unused))
        next = unused.pop(index)
        track[previous] = next
        previous = next
    return track


def find_weight(track, weights):
    weight = 0
    for i in range(0, len(track)):
        weight += weights[i][track[i]]
    return weight

if __name__ == "__main__":
    #######
    # parameters
    # define for the build in capital letter
    NBCITIES = 10
    NBELEMENTS = 256 # 256 = number max for a work group, any more element won't be sync with the other -> bug read/write
    SIZERANDOM = 1000
    PROBAMUT = 100
    PROBACROSS = 100
    GENERATIONS = 100

    min_weight = 1  # used for the radom track weight
    max_weight = int(sqrt(20))  # not for the map generation
    X = [-10, 10]  # map zone
    Y = [-10, 10]  # map zone
    display_map = True
    # end parameters
    #######

    #import os
    #os.environ['PYOPENCL_COMPILER_OUTPUT'] = '1'
    #init weight, elements and list cities

    if display_map: map, weight = generate_random_map(NBCITIES, X[1]+1, Y[1]+1, X[0], Y[0])
    else: weight = None ; map = None
    list_cities = []
    for i in range(0, NBCITIES):
        name = str(unichr(randrange(65, 90)))
        name += str(unichr(randrange(65, 90)))
        name += str(unichr(randrange(65, 90)))
        name += str(unichr(randrange(65, 90)))
        name += str(unichr(randrange(65, 90)))
        list_cities.append(name)
    name_cities = list_cities
    if weight is None: weights_cities = generated_random_weight(min_weight, max_weight, name_cities)
    else: weights_cities = weight
    tracks = []
    weight_tracks = []
    for i in range(0, NBELEMENTS):
        tracks.append(generate_random_hamiltonian_track(weights=weights_cities))
        weight_tracks.append(find_weight(tracks[-1], weights_cities))
    best_track = deepcopy(tracks[0])
    id_tracks = []
    for i in range(0, NBELEMENTS): id_tracks.append(i)
    randomTable = []
    for i in range(0, SIZERANDOM): randomTable.append(randrange(0, NBELEMENTS))
    indexRandom = [0]

    # init kernel + build prgm
    platform = cl.get_platforms()[0]
    device = platform.get_devices()[0]

    context = cl.Context([device])
    file = open("cl_prgm_element.c", "r")
    myprgm = file.read()
    file.close()
    define = """
        #pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable
        #pragma OPENCL EXTENSION cl_khr_local_int32_base_atomics : enable
        #pragma OPENCL EXTENSION cl_khr_global_int32_extended_atomics : enable
        #pragma OPENCL EXTENSION cl_khr_local_int32_extended_atomics : enable

        #define SIZERANDOM """+str(SIZERANDOM)+"""
        #define NBELEMENTS """+str(NBELEMENTS)+"""
        #define NBCITIES """+str(NBCITIES)+"""
        #define PROBACROSS """+str(PROBACROSS)+ """
        #define PROBAMUT """ + str(PROBAMUT)+"""
        #define GENERATIONS """ + str(GENERATIONS)+"""\n\n"""
    myprgm = define + myprgm
    program = cl.Program(context, myprgm).build()
    queue = cl.CommandQueue(context)

    # bufferise global variables into the gpu
    mem_flags = cl.mem_flags
    weights_cities = numpy.array(weights_cities, dtype=numpy.float32)
    weights_citiesB = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=weights_cities)
    tracks = numpy.array(tracks, dtype=numpy.int32)
    tracksB = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=tracks)
    weight_tracks = numpy.array(weight_tracks, dtype=numpy.float32)
    weight_tracksB = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=weight_tracks)
    id_tracks = numpy.array(id_tracks, dtype=numpy.int32)
    id_tracksB = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=id_tracks)
    randomTable = numpy.array(randomTable, dtype=numpy.int32)
    randomTableB = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=randomTable)
    indexRandom = numpy.array(indexRandom, dtype=numpy.int32)
    indexRandomB = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=indexRandom)
    #the buffer that'll hold the returned last best track
    best_track = numpy.array(best_track, dtype=numpy.int32)
    best_trackB = cl.Buffer(context, mem_flags.WRITE_ONLY, best_track.nbytes)
    kernel = program.generation
    print best_track
    print find_weight(best_track, weights_cities)
    print "kernel : "
    m_time = time.time();
    #kernel.set_scalar_arg_dtypes([None, None,None, int]) # used to pass direct value into kernel
    kernel(
        queue, # the queue where the prgm will be executed
        (NBELEMENTS+1,1), # the number of treads per work group
        None,
        weights_citiesB, # arguments of the opencl prgm
        tracksB,
        weight_tracksB,
        id_tracksB,
        best_trackB,
        randomTableB,
        indexRandomB
        )
    cl.enqueue_copy(queue, best_track, best_trackB)

    print "sortie (apres %s sec) : " % (time.time() - m_time)
    print(best_track)
    print find_weight(best_track, weights_cities)

    if map is not None:
        mapX = []
        mapY = []
        pathx =[]
        pathy =[]
        plot.figure(1)
        for point in map:
            mapX.append(point[0])
            mapY.append(point[1])
        previous = 0
        for i in range(0, len(best_track)):
            previous = best_track[previous]
            pathx.append(map[best_track[previous]][0])
            pathy.append(map[best_track[previous]][1])
        pathx.append(pathx[0])
        pathy.append(pathy[0])
        plot.plot(pathx, pathy, linestyle='-', marker='o')
        plot.axis([X[0]-1, X[1]+1, Y[0]-1, Y[1]+1])
        plot.plot(mapX, mapY, "ro")
        plot.draw()
        plot.show()
