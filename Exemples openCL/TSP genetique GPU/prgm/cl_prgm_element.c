float findWeight(__global const float* weights_cities, int *track)
{
    float weight = 0;
    for(int i = 0; i < NBCITIES; i++)
    {
        weight += weights_cities[i * NBCITIES + track[i]]; // equals : weights_cities[i*size+track[i]]
    }
    return weight;
}
int getRandom(__global int *randomTable, __global int *indexRandom, int min, int max, int seed)
{
    seed = (seed + 1) * (seed + 2);
    atomic_xor(indexRandom, (*indexRandom * seed + randomTable[seed]) % SIZERANDOM);
    return (randomTable[(*indexRandom * seed + seed) % SIZERANDOM] % max) + min;    
    
}
int getRandomUnusedIndex(bool *table, __global int *randomTable, __global int *indexRandom, int seed)
{
    int index = getRandom(randomTable, indexRandom, 0, NBCITIES, seed);
    while(!table[index]) index = (index+1) % NBCITIES;
    table[index] = false;
    return index;
}
int correctHamiltonianTrack(int *track, __global int *randomTable, __global int *indexRandom)
{
    int error = 0;
    bool ended = false;
    int  previous = 0;
    int next = track[0];
    int iterator = 0;
    bool unused[NBCITIES];
    for(int i = 0; i < NBCITIES; i++) unused[i] = true;
    while(!ended)
    {
        unused[previous] = false;
        iterator++;
        if(iterator >= NBCITIES)
        {
            track[previous] = 0;
            return error;
        }
        if(!unused[next] || (next == 0 && iterator < NBCITIES))
        {
            if(next == 0)
            {
                error++;
                next = getRandomUnusedIndex(unused, randomTable, indexRandom, iterator);
                track[previous] = next;
            }
            else
            {                    
                error++;
                next = getRandomUnusedIndex(unused, randomTable, indexRandom, iterator);
                track[previous] = next;
            }
        }
        previous = next;
        next = track[previous];        
    }
    return error;
}
void swap(float table[], int ids[], int a, int b)
{   
    float buff = table[a];
    int buff2 = ids[a];
    table[a] = table[b];
    table[b] = buff;
    ids[a] = ids[b];
    ids[b] = buff2;
}
int partition(float *arr, int *arr2, int l, int h)
{
    float x = arr[h];
    int i = (l - 1);
 
    for (int j = l; j <= h- 1; j++)
    {
        if (arr[j] <= x)
        {
            i++;
            swap(arr, arr2, i, j);
        }
    }
    swap(arr, arr2, i+1, h);
    return (i + 1);
}
void quickSort(float *arr, int *arr2, int l, int h)
{
    float stack[NBELEMENTS];
 
    int top = -1;
 
    stack[ ++top ] = l;
    stack[ ++top ] = h;
 
    while ( top >= 0 )
    {
        h = stack[ top-- ];
        l = stack[ top-- ];
 
        int p = partition( arr, arr2, l, h );
 
        if ( p-1 > l )
        {
            stack[ ++top ] = l;
            stack[ ++top ] = p - 1;
        }
 
        if ( p+1 < h )
        {
            stack[ ++top ] = p + 1;
            stack[ ++top ] = h;
        }
    }
}
void sortKernel(__global float *weight_tracks, __global const float *weights_cities, float *best_weight, __global int *tracks, 
    __global int *id_tracks, __global int *randomTable, __global int *indexRandom, 
    __global int *best_track)
{ 
    float m_table[NBELEMENTS];
    int ids[NBELEMENTS];       
    for (int i = 0; i < NBELEMENTS; i++)
    {
        ids[i] = i; // tocheck
        m_table[i] = weight_tracks[i];
    }
    quickSort(m_table, ids, 0, NBELEMENTS-1);
    if(*best_weight > m_table[0])
    {
        int buffBest[NBCITIES];
        for(int j = 0; j < NBCITIES; j++)
                {buffBest[j] = tracks[ids[0]*NBCITIES + j];}
        if(correctHamiltonianTrack(buffBest, randomTable, indexRandom) > 0) printf("erorB \n");
        if(findWeight(weights_cities, buffBest) < *best_weight)
        {
            for(int i = 0; i < NBCITIES ; i++)
            {
                best_track[i] = buffBest[i];
            }
            *best_weight = m_table[0];
        }        
    }
    for (int i = 0; i < NBELEMENTS; i++)
    {            
        weight_tracks[i] = m_table[i];
        id_tracks[i] = ids[i];
    }
}
int closestCity(__global const float* weights_cities, int from, int to0, int to1)
{
    if(weights_cities[from * NBCITIES + to0] < weights_cities[from * NBCITIES + to1])
    {
        return to0;
    }
    return to1;
}
int getThirtyPercentTrack(__global int *tracks, __global int *ids, int start)
{
    for(int i = 0; i < NBELEMENTS; i++)
    {
        if(ids[start] <= NBELEMENTS*30/100)
        {
            return start; 
        }
        start = (start + 1) % NBELEMENTS;
    }
    return 0;
}
void crossGreedy(__global const float *weights_cities, __global int *randomTable, __global int *indexRandom, int id, int *m_track, int *secondParent)
{
    bool unused[NBCITIES];
    for(int i = 0; i < NBCITIES; i++) unused[i]  = true;
    int previous = 0;
    int next;
    int iterator = 0;
    while(iterator < NBCITIES)
    {
        unused[previous] = false;
        iterator++;
        if(iterator >= NBCITIES)
        {
            m_track[previous] = 0;
            break;
        }
        else if(unused[m_track[previous]] && unused[secondParent[previous]])
        {
            next = closestCity(weights_cities, previous, m_track[previous], secondParent[previous]);
            m_track[previous] = next;
            previous = next;
        }
        else if(!unused[m_track[previous]] && unused[secondParent[previous]])
        {
            next = secondParent[previous];
            m_track[previous] = next;
            previous = next;
        }
        else if(!unused[m_track[previous]] && !unused[secondParent[previous]])
        {
            next = getRandomUnusedIndex(unused, randomTable, indexRandom, iterator);            
            m_track[previous] = next;
            previous = next;
        }
        else previous = m_track[previous]; 
    }    
}
void crossing(int id, __global const float* weights_cities, __global int *tracks, __global int *randomTable, 
    __global int *indexRandom, __global int *ids, int *m_track)
{    
    if(getRandom(randomTable, indexRandom, 0, 100, id) <= PROBACROSS)
    {
        int indexSecondParent = getThirtyPercentTrack(tracks, ids, getRandom(randomTable, indexRandom, 0, NBELEMENTS, id)); 
        int secondParent[NBCITIES];
        for(int i = 0; i < NBCITIES; i++)
        {
            secondParent[i] = tracks[indexSecondParent * NBCITIES + i];
        }
        if(correctHamiltonianTrack(secondParent, randomTable, indexRandom) > 0) printf("errorC ");
        crossGreedy(weights_cities, randomTable, indexRandom, id, m_track, secondParent);        
    }
}
void mutate(int id, int *m_track, __global int *randomTable, __global int *indexRandom)
{
    int previous = 0;
    int current = m_track[0];
    
    for(int i = 1; i < NBCITIES-1; i++)
    {
        if(getRandom(randomTable, indexRandom, 0, 100, id) <= PROBAMUT)
        {
            int next = m_track[current];
            m_track[previous] = next;            
            m_track[current] = m_track[next];
            m_track[next] = current;
            current = next;
        }
        previous = current;
        current = m_track[current];
    }
    
    if(correctHamiltonianTrack(m_track, randomTable, indexRandom) > 0) printf("errorM ");
}
void oneGeneration(int id, __global const float* weights_cities, __global int *tracks, __global float* weight_tracks,
    int *m_track, __global int *randomTable, __global int *indexRandom, __global int *id_tracks)
{
    crossing(id, weights_cities, tracks, randomTable, indexRandom, id_tracks, m_track);
    mutate(id, m_track, randomTable, indexRandom);
    
}
void oneGenerationSet(int id, __global const float* weights_cities, __global int *tracks, __global float* weight_tracks,
    int *m_track, __global int *randomTable, __global int *indexRandom, __global int *id_tracks)
{
    float m_weight = findWeight(weights_cities, m_track);
    weight_tracks[id] = m_weight;
    for(int i = 0; i < NBCITIES; i++) tracks[id * NBCITIES + i] = m_track[i];
}

__kernel void generation
(
    __global const float *weights_cities, /* 2D table filled with weight of the path city X to city Y */
    __global int *tracks, /* 2D table filled the track of each element oof the generation */
    __global float *weight_tracks, /* table filled (and sorted by thread 0) with the weight of each element */
    __global int *id_tracks, /* IDs sorted by weight */
    __global int *best_track, /* data to return after all the generations : the shortest path discovered */
    __global int *randomTable, /* table of random integers */
    __global int *indexRandom /* an index to the table of randoms that'll be increased any time it's used */
)
{
    int gid = get_global_id(0);  
    int m_track[NBCITIES];
    int id = gid;
    float best_weight;
    if(id != 0)
    {
        id--;
        for(int i = 0; i < NBCITIES; i++)
        {
            m_track[i] = tracks[id*NBCITIES+i];
        }
    }
    else
    {
        int bestBis[NBCITIES];
        for(int i = 0; i < NBCITIES; i++)
        {
            best_track[i] = tracks[i];
            bestBis[i] = tracks[i];
        }
        best_weight = findWeight(weights_cities, bestBis);
    }
    int generations = GENERATIONS;
    while(generations > 0)
    {     
        if(gid == 0)
        { // sort the weights and get the best track
            sortKernel(weight_tracks, weights_cities, &best_weight, tracks, id_tracks,
                randomTable, indexRandom, best_track);
        }
        else
        { // compute next generation    
            oneGeneration(id, weights_cities, tracks, weight_tracks, m_track, randomTable, indexRandom, id_tracks);
        }
        barrier(CLK_GLOBAL_MEM_FENCE);
        if(gid != 0)
        { // set the new generation
            oneGenerationSet(id, weights_cities, tracks, weight_tracks, m_track, randomTable, indexRandom, id_tracks);
        }
        barrier(CLK_GLOBAL_MEM_FENCE);        
        generations--;
    }
    
}