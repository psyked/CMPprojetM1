# La compression

## Le principe de Huffman :
http://www-igm.univ-mlv.fr/~lecroq/cours/huff.pdf  
remplacer les caractères les plus fréquents par des codes plus courts

#### Code préfix : 
ensemble de mots tel qu'aucun mot n'est préfixe d'un autre.  
Le décodage est alors immédiat pusiqu'aucune confusion n'est possible entre les mots. (Toujours le cas des codes de taille fixe, et une caractéristique recherchée dans le cas des codes à taille variable)  
https://fr.wikipedia.org/wiki/Code_pr%C3%A9fixe

#### L'arbre : 
pour encoder, on va construire un arbre, dont chaque feuille correspond à un caractère d'origine    
![Tree sample](tree.png "Exemple d'arbre pour la phrase 'this is an example of a huffman tree")  
En bas de l'arbre sont placés les caractères les moins présents. On leur associe un poids correspondant à leur nombre d'apparition, et on les regroupe par 2 en un noeud, et on répète l'opération avec des noeuds de poids de plus en plus élevé. Ensuite, par convention, on associe la branche de gauche d'un noeud à un 0 et celle de droite à un 1.  
https://fr.wikipedia.org/wiki/Codage_de_Huffman

http://stackoverflow.com/questions/456829/compression-library-using-nvidias-cuda  
http://www.wave-access.com/public_en/blog/2011/april/22/breakthrough-in-cuda-data-compression.aspx  
http://www.manavski.com/downloads/PID505889.pdf  

## Finalement : pas assez rentable - abandonné