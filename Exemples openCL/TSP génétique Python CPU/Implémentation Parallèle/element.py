from random import randrange
from copy import deepcopy


class Element:
    track = [] # list of tuples (start, destination)
    weight = 0 # weight of the track = score of the element
    second_track = None # the track of the element to be crossed with
    weights = [] # 2D table that storage the weight of the paths
    state = "not initialised" # debugging trace
    probability_mutation = 40 # does it really need to be explained ?
    probability_crossing = 40 # does it really need to be explained ?
    percent = 0 # the position (%) in the sorted list of elements

    def __init__(self, weights, track=None, probamut=40, probacross=40):
        self.state = "initialisation"
        self.weights = weights
        if track is None: self.track = self.generate_random_hamiltonian_track()
        else: self.track = track
        self.probability_mutation = probamut
        self.probability_crossing = probacross
        self.find_weight()
        pass

    def __str__(self, passline=False):
        string = "Element : "
        if passline: string += "\n"
        string += "state = " + str(self.state) + " "
        if passline: string += "\n"
        string += "weight : " + str(self.weight) + " "
        if passline: string += "\n"
        string += "proba mut/cross : " + str(self.probability_mutation) + " " + str(self.probability_crossing) + " "
        if passline: string += "\n"
        string += "track : "
        if passline: string += "\n"
        for edge in self.track:
            string += str(edge) + " "
            if passline: string += "\n"
        return string

    def __lt__(self, other):
        self.find_weight()
        return self.weight < other.weight

    def generate_random_hamiltonian_track(self, size=None):
        if size is None: size = len(self.weights)
        track = []
        previous = 0
        unused = []
        for i in range(0, size): unused.append(i)
        unused.pop(0)
        for i in range(0, size-1):
            if len(unused) != 1:
                destination = unused[randrange(0, len(unused)-1)]
            else: destination = unused[0]
            unused.pop(unused.index(destination))
            track.append((previous, destination))
            previous = destination
        track.append((track[-1][1], 0))
        return track

    def find_weight(self):
        self.weight = 0
        for edge in self.track:
            self.weight += self.weights[edge[0]][edge[1]]
        pass

    def shortest_edge(self, edge0, edge1):
        if self.weights[edge0[0]][edge0[1]] <= self.weights[edge1[0]][edge1[1]]:
            return edge0
        return edge1

    def crossgreedy(self, secondparenttrack):
        """chose the shortest path for each edge of each parent"""
        unused = []
        used = [0]
        track = []
        for edge in self.track:
            if edge[0] == 0: continue
            unused.append(edge[0])
        current = 0
        for i in range(0, len(unused)):
            # take the shortest path between the two parents if the destination isn't already used
            if (self.get_edge_from_start(current)[1] not in used
            and self.get_edge_from_start(current, secondparenttrack)[1] not in used):
                edge = self.shortest_edge(self.get_edge_from_start(current), self.get_edge_from_start(current, secondparenttrack))
            elif (self.get_edge_from_start(current)[1] not in used
            and self.get_edge_from_start(current, secondparenttrack)[1] in used):
                edge = self.get_edge_from_start(current)
            elif (self.get_edge_from_start(current)[1] in used
            and self.get_edge_from_start(current, secondparenttrack)[1] not in used):
                edge = self.get_edge_from_start(current, secondparenttrack)
            # take a random unused destination or the end of the path if no unused node are left
            else:
                edge = (current, unused[randrange(0, len(unused))])
            # change the current departure with the last destination, then pop the current from unused nodes list
            # and add it to the used list
            unused.pop(unused.index(edge[1]))
            used.append(edge[1])
            current = edge[1]
            track.append(edge)

        edge = (current, 0)
        track.append(edge)
        return track

    def get_edge_from_start(self, start, track=None):
        if track is None: track = self.track
        for edge in track:
            if edge[0] == start:
                return edge

    def mutate(self):
        iterator = 0
        for edge in self.track:
            if iterator <= 0 or iterator >= len(self.track) - 1: iterator += 1; continue
            if randrange(0, 100) <= self.probability_mutation:
                edge = (edge[1], edge[0])
                self.track[iterator] = edge
                self.track[iterator - 1] = (self.track[iterator - 1][0], edge[0])
                self.track[iterator + 1] = (edge[1], self.track[iterator + 1][1])
            iterator += 1
        self.find_weight()

    def get_weight(self):
        return self.weight

    def set_second_parent(self, secondtrack):
        self.second_track = secondtrack
        self.cross()
        self.mutate()

    def get_track(self):
        return deepcopy(self.track)

    def set_track(self, track):
        self.track = track

    def cross(self):
        """cross the element with its designated element
        and erase its track with the child one"""
        if randrange(0, 100) <= self.probability_crossing:
            track = self.crossgreedy(self.second_track)
            self.track = track
