from multiprocessing import Pool
from element import Element
from math import sqrt
from random import randrange
from copy import deepcopy
import matplotlib.pyplot as plot

def generate_random_map(nbpoints, maxx, maxy, minx=0, miny=0):
    if (maxx - minx) * (maxy - miny) < nbpoints: return None
    map = []
    weights = []
    # create map
    for i in range(0, nbpoints):
        point = None
        while point is None or point in map:
            point = (randrange(minx, maxx), randrange(miny, maxy))
        map.append(point)
    # compute weight map
    for i in range(0, nbpoints):
        line = []
        for j in range(0, nbpoints):
            if i == j:
                line.append(None)
            else:
                line.append(sqrt((map[i][0] - map[j][0]) ** 2 + (map[i][1] - map[j][1]) ** 2))
        weights.append(line)
    return map, weights


def generated_random_weight(wmin, wmax, names):
    weight = []
    for i in range(0, len(names)):
        line = []
        for j in range(0, len(names)):
            if i == j:
                line.append(None)
            else:
                line.append(randrange(wmin, wmax))
        weight.append(line)
    return weight


elements = []  # list of elements
best_solution = None
weights_cities = []
name_cities = []


def greedy_path():
    greedytrack = []
    current = 0
    unused = []
    i = 0
    for name in name_cities:
        i += 1
        unused.append(i)
    while len(unused) >= 2:
        best = None
        next = None
        for i in range(0, len(weights_cities[current])):
            if weights_cities[current][i] is None or i not in unused:
                continue
            elif best is None or best > weights_cities[current][i]:
                best = weights_cities[current][i]
                next = i
                continue
        greedytrack.append((current, next))
        current = next
        unused.pop(unused.index(current))
    greedytrack.append((greedytrack[-1][1], 0))

    path_greedy = Element(weights_cities, track=greedytrack)
    path_greedy.find_weight()
    return path_greedy

def set_parent(index):
    global elements
    element = elements[index]
    element.set_second_parent(deepcopy(elements[randrange(0, 30*len(elements)/100)].track))
    elements[index] = element
    return 0

def worker(i):
    return i


if __name__ == '__main__':
    #######
    # parameters
    nb_cities = 150
    size_generation = 50
    proba_mutation = 100
    proba_die_or_cross = 100
    number_generation = 2000
    min_weight = 20  # used for the radom track weight
    max_weight = 42  # not for the map generation
    X = [-10, 10]  # map zone
    Y = [-10, 10]  # map zone
    # end parameters
    #######

    map, weight = generate_random_map(nb_cities, X[1]-1, Y[1]-1, X[0]+1, Y[0]+1)
    #weight = None ; map = None

    list_cities = []
    for i in range(0, nb_cities):
        name = str(unichr(randrange(65, 90)))
        name += str(unichr(randrange(65, 90)))
        name += str(unichr(randrange(65, 90)))
        name += str(unichr(randrange(65, 90)))
        name += str(unichr(randrange(65, 90)))
        list_cities.append(name)

    name_cities = list_cities
    if weight is None: weights_cities = generated_random_weight(0,5, name_cities)
    else: weights_cities = weight
    for i in range(0, size_generation):
        elements.append(Element(weights=weights_cities, probacross=proba_die_or_cross, probamut=proba_mutation))
    elements.sort()
    best_solution = deepcopy(elements[0])
    best = best_solution.weight
    maxY = best + 1

    greedy_path = greedy_path()
    greedy_track = []
    for i in range(0, number_generation):
        greedy_track.append(greedy_path.weight)

    pool = Pool(processes=4)
    for index in range(0, size_generation):
        pool.apply_async(worker, (index,), callback=set_parent)
    pool.close()
    pool.join()
    elements.sort()
    best_solution = deepcopy(elements[0])
    print "Best : " + str(best_solution.weight)

    points = [[0], [best]]
    plot.figure(1)
    plot.plot(points[0], points[1], 'ro')
    plot.plot(greedy_track)
    plot.axis([-1, 11, -1, maxY])
    plot.ylabel("max length")
    plot.xlabel("generation")
    plot.ion()
    plot.subplot(111)
    plot.title("Best path in function of the generation")
    plot.figure(2)
    mapX = []
    mapY = []
    pathx = []
    pathy = []
    path_greedy = [[], []]

    if map is not None:
        for point in map:
            mapX.append(point[0])
            mapY.append(point[1])
        for path in greedy_path.track:
            path_greedy[0].append(map[path[0]][0])
            path_greedy[1].append(map[path[0]][1])
        path_greedy[0].append(path_greedy[0][0])
        path_greedy[1].append(path_greedy[1][0])
        plot.plot(path_greedy[0], path_greedy[1], 'r:')
        plot.ion()
        plot.axis([X[0]-1, X[1]+1, Y[0]-1, Y[1]+1])
        plot.plot(mapX, mapY, "ro")
        plot.draw()
        plot.show()

    best = best_solution.weight

    for i in range(1, number_generation):
        pool = Pool(processes=4)
        for index in range(0, size_generation):
            pool.apply_async(worker, (index,), callback=set_parent)
        pool.close()
        pool.join()
        elements.sort()
        best_solution = deepcopy(elements[0])

        if best > best_solution.weight:
            best = best_solution.weight
            print "new best at generation %s : %s" % (i, best)

            plot.figure(1)
            plot.clf()
            points[0].append(i)
            points[1].append(best)
            plot.plot(points[0], points[1])
            plot.plot(points[0], points[1], "ro")
            plot.plot(greedy_track)
            plot.axis([0, i, 0, maxY])
            plot.subplot(111)
            plot.ylabel("max length")
            plot.xlabel("generation")
            plot.title("Best path in function of the generation")
            if map is not None:
                plot.figure(2)
                plot.clf()
                plot.axis([X[0], X[1], Y[0], Y[1]])
                plot.plot(mapX, mapY, "ro")
                pathx =[]
                pathy =[]
                for path in best_solution.track:
                    pathx.append(map[path[0]][0])
                    pathy.append(map[path[0]][1])
                pathx.append(pathx[0])
                pathy.append(pathy[0])
                plot.plot(pathx, pathy, linestyle='-', marker='o')
                for path in greedy_path.track:
                    path_greedy[0].append(map[path[0]][0])
                    path_greedy[1].append(map[path[0]][1])
                path_greedy[0].append(path_greedy[0][0])
                path_greedy[1].append(path_greedy[1][0])
                plot.plot(path_greedy[0], path_greedy[1], 'r:')
                plot.draw()

            plot.draw()
            plot.pause(0.001)
    plot.figure(1)
    plot.clf()
    points[0].append(number_generation)
    points[1].append(best)
    plot.plot(points[0], points[1], "ro")
    plot.plot(greedy_track)
    plot.plot(points[0], points[1])
    plot.subplot(111)
    plot.axis([0, number_generation, 0, maxY])
    plot.ylabel("max length")
    plot.xlabel("generation")
    plot.title("Best path in function of the generation")
    plot.draw()

    if map is not None:
        plot.figure(2)
        plot.clf()
        plot.axis([X[0], X[1], Y[0], Y[1]])
        plot.plot(mapX, mapY, "ro")
        pathx =[]
        pathy =[]
        for path in best_solution.track:
            pathx.append(map[path[0]][0])
            pathy.append(map[path[0]][1])
        pathx.append(pathx[0])
        pathy.append(pathy[0])
        plot.plot(pathx, pathy, linestyle='-', marker='o')
        for path in greedy_path.track:
            path_greedy[0].append(map[path[0]][0])
            path_greedy[1].append(map[path[0]][1])
        path_greedy[0].append(path_greedy[0][0])
        path_greedy[1].append(path_greedy[1][0])
        plot.plot(path_greedy[0], path_greedy[1], 'r:')
        plot.draw()

    plot.pause(1)
    inp = input("press to quit")
