from element_genetic import TrackElement
from copy import deepcopy
from random import randrange
import matplotlib.pyplot as plot
import element_genetic
from math import sqrt


def generate_random_map(nbpoints, maxx, maxy, minx=0, miny=0):
    if (maxx - minx) * (maxy - miny) < nbpoints: return None
    map = []
    weights = []
    # create map
    for i in range(0, nbpoints):
        point = None
        while point is None or point in map:
            point = (randrange(minx, maxx), randrange(miny, maxy))
        map.append(point)
    # compute weight map
    for i in range(0, nbpoints):
        line = []
        for j in range(0, nbpoints):
            if i == j:
                line.append(None)
            else:
                line.append(sqrt((map[i][0] - map[j][0]) ** 2 + (map[i][1] - map[j][1]) ** 2))
        weights.append(line)
    return map, weights


def generated_random_weight(wmin, wmax, names):
    weight = []
    for i in range(0, len(names)):
        line = []
        for j in range(0, len(names)):
            if i == j:
                line.append(None)
            else:
                line.append(randrange(wmin, wmax))
        weight.append(line)
    return weight


class GenerationGenetic:
    size_population = 0
    name_cities = []  # name cities
    weight_cities = [[]]  # weight of the edge i->j en [i][j]
    generation = []  # list of all elements
    best_solution = None
    crossing_probability = 10  # define the proportion changed in the population by crossing/killing

    def __str__(self, passline=False, showweight=False, showelements=False, debugelem=False, showbest=False):
        string = "Generation : "
        if passline: string += "\n"
        string += "size population : " + str(self.size_population) + " "
        if passline: string += "\n"
        string += "name cities : " + str(self.name_cities) + " "
        if passline: string += "\n"
        if showweight:
            string += "poids : "
            if passline: string += "\n"
            for elem in self.weight_cities:
                string += str(elem) + " "
                if passline: string += "\n"
        # why the f**k my 1rst item isn't sorted if i don't read all items befor ???
        newstring = str(self.generation[0])
        self.generation.sort()
        if self.best_solution is None: self.best_solution = deepcopy(self.generation[0])
        if showelements:
            for elem in self.generation:
                if not debugelem: string += elem.__str__(False, False, False) + "\n"
                else: string += elem.__str__(True, True, True, True, True) + "\n"
        if passline: string += "\n"
        if not showbest: string += "best : " + str(self.best_solution.weight_total) + " "
        else: string += "best : " + self.best_solution.__str__(True, True, True, True, False)
        if passline: string += "\n"
        return string

    def __init__(self, sizegeneration, names, weight=None, probamut=25, proportiongeneration=25, minweight=0,
                 maxweight=5):
        self.size_population = sizegeneration
        self.name_cities = names
        element_genetic.name_cities=names
        if weight is not None:
            self.weight_cities = weight
            element_genetic.weight_cities = weight
        else:
            self.weight_cities = self.generated_random_weight(minweight, maxweight+1)
            element_genetic.weight_cities = self.weight_cities
        # initialisation first generation
        for i in range(0, sizegeneration):
            self.generation.append(TrackElement(proba=probamut))
        self.mutation_probability = probamut
        self.crossing_probability = proportiongeneration
        str(self)
        pass

    def generated_random_weight(self, min, max):
        weight = []
        for i in range(0, len(self.name_cities)):
            line = []
            for j in range(0, len(self.name_cities)):
                if i == j:
                    line.append(None)
                else:
                    line.append(randrange(min, max))
            weight.append(line)
        return weight

    def new_generation(self):
        """build new generation"""
        # sort element by total weight of the path
        self.generation.sort()
        if self.best_solution is not None and len(self.generation) >= 1:
            if self.best_solution.weight_total > self.generation[0].weight_total:
                self.best_solution = deepcopy(self.generation[0])

        # cross some elements with the best 30%
        newgeneration = 0
        for i in range(0, self.size_population):
            if randrange(0, 100) <= self.crossing_probability:
                newgeneration += 1
                # cross it with a random element with a probability of 50% of crossing with one of the best 30%
                percent = randrange(0, 30)
                index = int(percent*(self.size_population-1)/100)
                self.generation.append(self.generation[i].crossgreedy(self.generation[index]))

        # eliminate some of the worst elements
        while len(self.generation) >= self.size_population:
            self.generation.pop(randrange(0, len(self.generation) - newgeneration))
            newgeneration -= 1

        # mutate some elements
        for elem in self.generation:
            elem.mutate()

#######
# parameters
nb_cities = 80
size_generation = 150
proba_mutation = 100
proba_die_or_cross = 100
number_generation = 1000
min_weight = 20 # used for the radom track weight
max_weight = 42 # not for the map generation
X = [-10, 10]
Y = [-10, 10]
# end parameters
#######

#generate map + weight
#map, weight = generate_random_map(nb_cities, X[1], Y[1], X[0], Y[0])
# generate weight without map
weight = None; map = None

list_cities = []
for i in range(0, nb_cities):
    name = str(unichr(randrange(65, 90)))
    name += str(unichr(randrange(65, 90)))
    name += str(unichr(randrange(65, 90)))
    name += str(unichr(randrange(65, 90)))
    name += str(unichr(randrange(65, 90)))
    list_cities.append(name)

a = GenerationGenetic(sizegeneration=size_generation, proportiongeneration=20, names=list_cities, probamut=proba_mutation
                      , weight=weight, minweight=1, maxweight=int(sqrt(200)))

best = a.best_solution.weight_total
maxY = best+1
greedy_path = element_genetic.greedy_path()
greedy_track =[]
for i in range(0, number_generation):
    greedy_track.append(greedy_path.weight_total)

points = [[0], [best]]
plot.figure(1)
plot.plot(points[0], points[1], 'ro')
plot.plot(greedy_track)
plot.axis([0, 10, 0, maxY])
plot.ylabel("max length")
plot.xlabel("generation")
plot.ion()
plot.subplot(111)
plot.title("Best path in function of the generation")

plot.figure(2)
mapX = []
mapY = []
pathx = []
pathy = []
path_greedy = [[], []]
if map is not None:
    for point in map:
        mapX.append(point[0])
        mapY.append(point[1])
    for path in greedy_path.track:
        path_greedy[0].append(map[path[0]][0])
        path_greedy[1].append(map[path[0]][1])
    path_greedy[0].append(path_greedy[0][0])
    path_greedy[1].append(path_greedy[1][0])
    plot.plot(path_greedy[0], path_greedy[1], 'r:')
    plot.ion()
    plot.axis([X[0], X[1], Y[0], Y[1]])
    plot.plot(mapX, mapY, "ro")
    plot.draw()
    plot.show()

for i in range(1, number_generation):
    a.new_generation()
    if best > a.best_solution.weight_total:
        best = a.best_solution.weight_total
        print "new best at generation %s : %s" % (i, best)
        plot.figure(1)
        plot.clf()
        points[0].append(i)
        points[1].append(best)
        plot.plot(points[0], points[1])
        plot.plot(points[0], points[1], "ro")
        plot.plot(greedy_track)
        plot.axis([0, i, 0, maxY])
        plot.subplot(111)
        plot.ylabel("max length")
        plot.xlabel("generation")
        plot.title("Best path in function of the generation")
        if map is not None:
            plot.figure(2)
            plot.clf()
            plot.axis([X[0], X[1], Y[0], Y[1]])
            plot.plot(mapX, mapY, "ro")
            pathx =[]
            pathy =[]
            for path in a.best_solution.track:
                pathx.append(map[path[0]][0])
                pathy.append(map[path[0]][1])
            pathx.append(pathx[0])
            pathy.append(pathy[0])
            plot.plot(pathx, pathy, linestyle='-', marker='o')
            for path in greedy_path.track:
                path_greedy[0].append(map[path[0]][0])
                path_greedy[1].append(map[path[0]][1])
            path_greedy[0].append(path_greedy[0][0])
            path_greedy[1].append(path_greedy[1][0])
            plot.plot(path_greedy[0], path_greedy[1], 'r:')
            plot.draw()

        plot.draw()
        plot.pause(0.001)
plot.figure(1)
plot.clf()
points[0].append(number_generation)
points[1].append(best)
plot.plot(points[0], points[1], "ro")
plot.plot(greedy_track)
plot.plot(points[0], points[1])
plot.subplot(111)
plot.axis([0, number_generation, 0, maxY])
plot.ylabel("max length")
plot.xlabel("generation")
plot.title("Best path in function of the generation")
plot.draw()

if map is not None:
    plot.figure(2)
    plot.clf()
    plot.axis([X[0], X[1], Y[0], Y[1]])
    plot.plot(mapX, mapY, "ro")
    pathx =[]
    pathy =[]
    for path in a.best_solution.track:
        pathx.append(map[path[0]][0])
        pathy.append(map[path[0]][1])
    pathx.append(pathx[0])
    pathy.append(pathy[0])
    plot.plot(pathx, pathy, linestyle='-', marker='o')
    for path in greedy_path.track:
        path_greedy[0].append(map[path[0]][0])
        path_greedy[1].append(map[path[0]][1])
    path_greedy[0].append(path_greedy[0][0])
    path_greedy[1].append(path_greedy[1][0])
    plot.plot(path_greedy[0], path_greedy[1], 'r:')
    plot.draw()

plot.pause(1)
inp = input("press to quit")
