from random import randrange
import copy


#static

weight_cities = []
name_cities = []


def greedy_path():
    greedytrack = []
    current = 0
    unused = []
    i = 0
    for name in name_cities:
        i += 1
        unused.append(i)
    while len(unused) >= 2:
        best = None
        next = None
        for i in range(0, len(weight_cities[current])):
            if weight_cities[current][i] is None or i not in unused: continue
            elif best is None or best > weight_cities[current][i]:
                best = weight_cities[current][i]
                next = i
                continue
        greedytrack.append((current, next))
        current = next
        unused.pop(unused.index(current))
    greedytrack.append((greedytrack[-1][1], 0))

    path_greedy = TrackElement(proba=0, track=greedytrack)
    path_greedy.find_weight()
    return path_greedy


def is_hamiltonian(track):
    dictionary_count = {}
    for edge in track:
        if edge[0] in dictionary_count:
            dictionary_count[edge[0]] += 1
        else:
            dictionary_count[edge[0]] = 1
        if edge[1] in dictionary_count:
            dictionary_count[edge[1]] += 1
        else:
            dictionary_count[edge[1]] = 1

    for key, value in dictionary_count.iteritems():
        if value != 2:
            return False

    return True

# end statics


class TrackElement:
    def __init__(self, proba=10, track=None):
        if track is not None: self.track = track
        elif name_cities is not None: self.track = self.generate_random_hamiltonian_track(len(name_cities))
        self.probability_mutation = proba
        pass

    def __str__(self, passline=False, returndescription=True, returnproba=True, returnweight=True, returnpath=True):
        self.find_weight()
        output = ""
        if returndescription:
            output += "TrackElement : "
            if passline: output += "\n"
        if returnproba:
            output += "probability of mutation : " + str(self.probability_mutation) + "% "
            if passline: output += "\n"
        if returnweight:
            output += "Weight : " + str(self.weight_total) + " "
            if passline: output += "\n"
        if returnpath:
            output += "Path : "
            if passline: output += "\n"
            for edge in self.track:
                output += name_cities[edge[0]] + " -> " + name_cities[edge[1]] + " "
                if passline: output += "\n"
        return output

    def __lt__(self, other):
        self.find_weight()
        return self.weight_total < other.weight_total

    def shortest_edge(self, edge0, edge1):
        if weight_cities[edge0[0]][edge0[1]] <= weight_cities[edge1[0]][edge1[1]]:
            return edge0
        return edge1

    def generate_random_hamiltonian_track(self, size):
        track = []
        previous = 0
        unused = []
        for i in range(0, size): unused.append(i)
        unused.pop(0)
        for i in range(0, size-1):
            if len(unused) != 1:
                destination = unused[randrange(0, len(unused)-1)]
            else: destination = unused[0]
            unused.pop(unused.index(destination))
            track.append((previous, destination))
            previous = destination
        track.append((track[-1][1], 0))
        return track

    def set_track(self, track):
        self.track = track
        pass

    def get_weight(self):
        self.find_weight()
        return self.weight_total

    def find_weight(self):
        self.weight_total = 0
        for edge in self.track:
            self.weight_total += weight_cities[edge[0]][edge[1]]
        pass

    def get_edge_from_start(self, start):
        for edge in self.track:
            if edge[0] == start:
                return edge

    def mutate(self):
        """use copy.deepcopy() to get a "child" then mutate it
        probability in percent"""
        iterator = 0
        for edge in self.track:
            if iterator == 0 or iterator == len(self.track) - 1: iterator += 1; continue
            if randrange(0, 100) <= self.probability_mutation:
                edge = (edge[1], edge[0])
                self.track[iterator] = edge
                self.track[iterator - 1] = (self.track[iterator - 1][0], edge[0])
                self.track[iterator + 1] = (edge[1], self.track[iterator + 1][1])
            iterator += 1

   # def cross(self, secondparent, probabilitysecondparent):
    #    """second parent is another TrackElement
    #    its probability of sharing its track is in percent"""
    #    usedpoints = [0]
    #    parentused = secondparent
    #    child = copy.deepcopy(parentused)
    #    for i in range(0, len(child.track)):
    #        if randrange(0, 100) <= probabilitysecondparent:
                # do nothing if the destination of both ith edges are equals
    #            if child.track[i][1] == parentused.track[i][1]: continue
                # change the destination of the ith edge if the destination isn't already used
    #            if parentused.track[i][1] in usedpoints:
    #                continue
    #            usedpoints.append(parentused.track[i][1])
    #            child.track[i] = (child.track[i][0], parentused.track[i][1])
    #            child.track[i+1] = (parentused.track[i + 1][0],  child.track[i + 1][1])

    #            return child


    def crossgreedy(self, secondparent):
        """chose the shortest path for each edge of each parent"""
        unused = []
        used = [0]
        track = []
        child = copy.deepcopy(secondparent)
        for edge in self.track:
            if edge[0] == 0: continue
            unused.append(edge[0])
        current = 0
        for i in range(0, len(unused)):
            # take the shortest path between the two parents if the destination isn't already used
            if (self.get_edge_from_start(current)[1] not in used
            and secondparent.get_edge_from_start(current)[1] not in used):
                edge = self.shortest_edge(self.get_edge_from_start(current), secondparent.get_edge_from_start(current))
            elif (self.get_edge_from_start(current)[1] not in used
            and secondparent.get_edge_from_start(current)[1] in used):
                edge = self.get_edge_from_start(current)
            elif (self.get_edge_from_start(current)[1] in used
            and secondparent.get_edge_from_start(current)[1] not in used):
                edge = secondparent.get_edge_from_start(current)
            # take a random unused destination or the end of the path if no unused node are left
            else:
                edge = (current, unused[randrange(0, len(unused))])
            # change the current departure with the last destination, then pop the current from unused nodes list
            # and add it to the used list
            unused.pop(unused.index(edge[1]))
            used.append(edge[1])
            current = edge[1]
            track.append(edge)

        edge = (current, 0)
        track.append(edge)
        child.set_track(track)
        return child

    track = [()]  # list of tuples (depart, destination)
    weight_total = 0
    probability_mutation = 10



if __name__ == '__main__':
    namestest = ["A", "B", "C", "D", "E"]
    maxrand = 5
    weighttest = [
        [None, randrange(0, maxrand), randrange(0, maxrand), randrange(0, maxrand), randrange(0, maxrand)],
        [randrange(0, maxrand), None, randrange(0, maxrand), randrange(0, maxrand), randrange(0, maxrand)],
        [randrange(0, maxrand), randrange(0, maxrand), None, randrange(0, maxrand), randrange(0, maxrand)],
        [randrange(0, maxrand), randrange(0, maxrand), randrange(0, maxrand), None, randrange(0, maxrand)],
        [randrange(0, maxrand), randrange(0, maxrand), randrange(0, maxrand), randrange(0, maxrand), None]
    ]
    weight_cities = weighttest
    name_cities = namestest


    trackelem0 = TrackElement(proba=50)
    trackelem1 = TrackElement(proba=50)

    print trackelem0
    print trackelem1
    c = trackelem0.crossgreedy(trackelem1)
    print is_hamiltonian(c.track)
    print c

    pass
