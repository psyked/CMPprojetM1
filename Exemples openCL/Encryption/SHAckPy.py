from Crypto.Hash import SHA256
from timeit import default_timer as timer

# Generation d'un hash (a partir d'un mot connu pour pouvoir verifier le bon fonctionnement de l'algorithme
word = "bana"
cible = SHA256.new(word)
target_length = len(word)
passwd = cible.hexdigest()
print passwd
print target_length

# Generation d'un alphabet
alphabet_start = 'a'
alphabet_end = 'z'
alphabet_range = ord(alphabet_end) - ord(alphabet_start) + 1
alphabet = []
for i in range(alphabet_range):
    alphabet.append(chr(ord(alphabet_start) + i))
print alphabet

# Generation du mot de depart et du mot de fin (condition du while)
test = []
end = []
indices = []
for i in range(target_length):
    test.append(alphabet_start)
    end.append(alphabet_end)
    indices.append(0)


# fonction pour passer au mot suivant
def increment(tab, indice):
    if indices[indice] < alphabet_range - 1:
        indices[indice] += 1
        depassement = False
    else:
        indices[indice] = 0
        depassement = True
    tab[indice] = alphabet[indices[indice]]
    return depassement


def from_tab_to_string(tab):
    out = ""
    for car in tab:
        out += car
    return out

start = timer()
while test != end:
    mes = from_tab_to_string(test)
    output = SHA256.new(mes)
    if output.hexdigest() == passwd:
        break
    execute = True
    j = target_length - 1
    while j >= 0:
        if execute:
            execute = increment(test, j)
        j -= 1
end = timer()

print mes
print end-start




