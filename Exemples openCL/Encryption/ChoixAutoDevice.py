from __future__ import absolute_import
import pyopencl as cl


def auto_device_selection():
    """Simple function selecting automatically the best available target platform for running an openCL kernel
    The order is Nvidia > ATI > Apple > Intel, as the Intel one is generally using processor (host) power
    Returns the first device of this platform"""
    intel_device = 0
    apple_device = 0
    ati_device = 0
    cuda_device = 0
    platforms = cl.get_platforms()
    for platform in platforms:
        if platform.name == "Intel(R) OpenCL":
            devices = platform.get_devices()
            intel_device = devices[0]

        if platform.name == "Apple":
            devices = platform.get_devices()
            apple_device = devices[0]

        if platform.name == "ATI Stream":
            devices = platform.get_devices()
            ati_device = devices[0]

        if platform.name == "NVIDIA CUDA":
            devices = platform.get_devices()
            cuda_device = devices[0]

    select_order = [intel_device, apple_device, ati_device, cuda_device]
    for value in select_order:
        if value != 0:
            selected_device = value
    print (selected_device)
    return selected_device

device = auto_device_selection()
