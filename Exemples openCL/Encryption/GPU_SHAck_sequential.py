from __future__ import absolute_import
from timeit import default_timer as timer
from Crypto.Hash import SHA256

import ChoixAutoDevice
import numpy as np
import pyopencl as cl

# dans le cas de l'utilisation sur CPU : permet d'afficher les infos de debug du compilateur
import os
os.environ['PYOPENCL_COMPILER_OUTPUT'] = '1'

# definition du hash et de la longueur du password
word = "maman"
cible = SHA256.new(word)
hashcode = cible.hexdigest()
passlen = len(word)
message = np.ones(passlen).astype(np.uint32)


device = ChoixAutoDevice.auto_device_selection()
# device = cl.get_platforms()[1].get_devices()[1] # Forcing Intel CPU for OpenCL
context = cl.Context([device])
kernel = """
#ifndef uint32_t
#define uint32_t unsigned int
#endif

#define H0 0x6a09e667
#define H1 0xbb67ae85
#define H2 0x3c6ef372
#define H3 0xa54ff53a
#define H4 0x510e527f
#define H5 0x9b05688c
#define H6 0x1f83d9ab
#define H7 0x5be0cd19

uint rotr(uint x, int n) {
    if (n < 32) return (x >> n) | (x << (32 - n));
    return x;
}
uint ch(uint x, uint y, uint z) {
    return (x & y) ^ (~x & z);
}
uint maj(uint x, uint y, uint z) {
    return (x & y) ^ (x & z) ^ (y & z);
}
uint sigma0(uint x) {
    return rotr(x, 2) ^ rotr(x, 13) ^ rotr(x, 22);
}
uint sigma1(uint x) {
    return rotr(x, 6) ^ rotr(x, 11) ^ rotr(x, 25);
}
uint gamma0(uint x) {
    return rotr(x, 7) ^ rotr(x, 18) ^ (x >> 3);
}
uint gamma1(uint x) {
    return rotr(x, 17) ^ rotr(x, 19) ^ (x >> 10);
}

uint sha256_crypt_kernel( char *plain_key, __global uint *hash_tab, uint *K){
    uint digest[] = {H0, H1, H2, H3, H4, H5, H6, H7};
    int t, gid, msg_pad;
    int stop, mmod;
    uint i, ulen, item, total;
    uint W[80], temp, A,B,C,D,E,F,G,H,T1,T2;
    uint num_keys = pass_block;
    int current_pad;

    msg_pad=0;
    ulen = pass_len;
    total = ulen%64>=56?2:1 + ulen/64;
    for(item=0; item<total; item++)
    {
        A = digest[0];
        B = digest[1];
        C = digest[2];
        D = digest[3];
        E = digest[4];
        F = digest[5];
        G = digest[6];
        H = digest[7];
        for (t = 0; t < 80; t++){
        W[t] = 0x00000000;
        }
        msg_pad=item*64;
        if(ulen > msg_pad)
        {
            current_pad = (ulen-msg_pad)>64?64:(ulen-msg_pad);
        }
        else
        {
            current_pad =-1;
        }

        if(current_pad>0)
        {
            i=current_pad;
            stop =  i/4;
            for (t = 0 ; t < stop ; t++){
                W[t] = ((uchar)  plain_key[msg_pad + t * 4]) << 24;
                W[t] |= ((uchar) plain_key[msg_pad + t * 4 + 1]) << 16;
                W[t] |= ((uchar) plain_key[msg_pad + t * 4 + 2]) << 8;
                W[t] |= (uchar)  plain_key[msg_pad + t * 4 + 3];
            }
            mmod = i % 4;
            if ( mmod == 3){
                W[t] = ((uchar)  plain_key[msg_pad + t * 4]) << 24;
                W[t] |= ((uchar) plain_key[msg_pad + t * 4 + 1]) << 16;
                W[t] |= ((uchar) plain_key[msg_pad + t * 4 + 2]) << 8;
                W[t] |=  ((uchar) 0x80) ;
            } else if (mmod == 2) {
                W[t] = ((uchar)  plain_key[msg_pad + t * 4]) << 24;
                W[t] |= ((uchar) plain_key[msg_pad + t * 4 + 1]) << 16;
                W[t] |=  0x8000 ;
            } else if (mmod == 1) {
                W[t] = ((uchar)  plain_key[msg_pad + t * 4]) << 24;
                W[t] |=  0x800000 ;
            } else /*if (mmod == 0)*/ {
                W[t] =  0x80000000 ;
            }

            if (current_pad<56)
            {
                W[15] =  ulen*8 ;
            }
        }
        else if(current_pad <0)
        {
            if( ulen%64==0)
                W[0]=0x80000000;
                W[15]=ulen*8;
        }
        for (t = 0; t < 64; t++) {
            if (t >= 16){
                W[t] = gamma1(W[t - 2]) + W[t - 7] + gamma0(W[t - 15]) + W[t - 16];
            }
            T1 = H + sigma1(E) + ch(E, F, G) + K[t] + W[t];
            T2 = sigma0(A) + maj(A, B, C);
            H = G; G = F; F = E; E = D + T1; D = C; C = B; B = A; A = T1 + T2;
        }
        digest[0] += A;
        digest[1] += B;
        digest[2] += C;
        digest[3] += D;
        digest[4] += E;
        digest[5] += F;
        digest[6] += G;
        digest[7] += H;
    }
    int test = 1;
    for(int i = 0; i < 8; i++){
        if(hash_tab[i] != digest[i]){
            test = 0;
        }
    }
    return test;

}


void copy_output( char *src, __global uint *dest){
    for(int i = 0; i < pass_len; i++){
        dest[i] = src[i];
    }
}

int increment(__global char *alphabet, char *word,__local int *indices, __local int *indice){
    int depassement = 0;
    if(indices[*indice] < alphabet_len - 1) {
        indices[*indice] += 1;
    } else {
        indices[*indice] = 0;
        depassement = 1;
    }
    word[*indice] = alphabet[indices[*indice]];
    return depassement;
}

__kernel void shack_256_kernel(__global char *alphabet, __global uint *hash_tab, __global uint *output){
    uint K[64]={
0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};
    char testword[pass_len];
    __local int indices[pass_len + 1];
    __local int count_char, execute, i;

    // creer le premier mot
    for(i = 0; i < pass_len; i++){
        testword[i] = alphabet[0];
        indices[i] = 0;
    }

    uint check = sha256_crypt_kernel(testword, hash_tab, K);
    while(check == 0){
        execute = 1;
        count_char = pass_len - 1;
        while(count_char >= 0){
            if (execute) {
                execute = increment(alphabet, testword, indices, &count_char);
            }
            count_char -= 1;
        }
        // printf("%s\\n", testword);
        check = sha256_crypt_kernel(testword, hash_tab, K);
    }
    barrier(CLK_GLOBAL_MEM_FENCE);
    barrier(CLK_LOCAL_MEM_FENCE);
    //printf("%s", testword);
    copy_output(testword, output);
}

        """
kernel_vars = """
#define pass_len %(passlen)u
#define alphabet_len %(alphabetrange)d
#define pass_block 1
"""

queue = cl.CommandQueue(context)
mem_flags = cl.mem_flags

# Generation d'un alphabet
alphabet_start = 'a'
alphabet_end = 'z'
alphabet_range = ord(alphabet_end) - ord(alphabet_start) + 1
alphabet = []
for i in range(alphabet_range):
    alphabet.append(chr(ord(alphabet_start) + i))
alphabet = np.array(alphabet)
print alphabet

# Convertir le hash string en np.array
hash_array = []
for i in range(8):
    temp_str = hashcode[(i*8):8*(i+1)]
    temp_int = int(temp_str, base=16)
    hash_array.append(temp_int)
hash_array = np.array(hash_array).astype(np.uint32)
print hash_array

program = cl.Program(context, (kernel_vars % {"passlen": passlen, "alphabetrange": alphabet_range} + kernel)).build()

alphabet_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=alphabet)
hash_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=hash_array)
output_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, message.nbytes)

start = timer()
completeEvent = program.shack_256_kernel(queue, (1, ), (1, ), alphabet_buf, hash_buf, output_buf)
events = [completeEvent]
# completeEvent.wait()
cl.enqueue_copy(queue, message, output_buf).wait()
end = timer()

# Get back password
password = ""
for i in message:
    password += chr(i)
print " | password is : " + password
print end-start





