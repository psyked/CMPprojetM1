#Sources
http://enja.org/2011/02/22/adventures-in-pyopencl-part-1-getting-started-with-python/

#Etapes

###Device
Choisir le bon périphérique. Nous avons créé une fonction pour le faire automatiquement

###Context

Créer un context:

    context = cl.Context([device])
   
###Ecriture du kernel

    kernel = """
    
    __kernel void function_name(args...)
    
      """
    
Y écrire fonctions qui seront executées sur le processeur GPU.

###Créer une file d'attente

    queue = cl.CommandQueue(context)
    
###Créer le programme
Autrement dit, compiler le noyau pour le context déclaré ci-dessus

    program = cl.Program(context, kernel).build()
    
###Définition des variables
Dans un premier temps, il faut les définir côté host pour pouvoir ensuite créer des buffers pour allouer la mémoire du côté device pour les varibles d'entrée et de sortie.

    input_variable = ....
    buffer_input = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf= input_variable)
    buffer_output = cl.Buffer(context, cl.mem_flags.WRITE_ONLY, size)
    
`size`: nombre d'octets nécessaires pour coder la variable.
    
###Appel du kernel

    program.function_name(queue, global_worksize, local_worksize, I/O buffers....)
`local_worksize` peut être laissé à "None" afin qu'il soit calculé automatiquement. Limité à 1024 avec une GTX970M

`global_worksize` : pas de limite de taille

###Récupération du buffer de sortie

    cl.enqueue_copy(queue, output, buffer_output)
    
Le résultat sera ainsi stocké dans la variable 'output'

![OpenCLSteps.png](OpenCLSteps.png "")