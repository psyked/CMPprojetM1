# Le cryptage : AES
http://www.manavski.com/downloads/PID505889.pdf


## Highly parallelisable block cipher  
https://www.lri.fr/~fmartignon/documenti/systemesecurite/5-AES.pdf  
Block size : 128  
key length : 128/192/256  
Nb of rounds : 10/12/14  
~key scheduling : 44/52/60 subkey of 32bits  

## 4 Steps per rounds
• SubBytes (byte-by-byte substitution using an S-box)  
• ShiftRows (a permutation, which cyclically shifts the last three rows in the State)  
• MixColumns (substitution that uses Galois Fields, corps de Galois, GF(28) arithmetic)  
• AddRound key (bit-by- bit XOR with an expanded key)

## Matrix representation
#### The key
Matrix 4xN, with N = key length / 32
#### The block
Matrix 4xM with M = block length / 32
#### The state
the AES algorithm's operations are performed on a 2D array of BYTES  
size 4xM

#####Finalement, AES algorithme compliqué, nous avons privilégié un algorithme de hashage

https://www.tutorialspoint.com/cryptography/advanced_encryption_standard.htm


# Le hashage : SHA-256
Essayer de craquer un hash


### Code tout fait en C
https://github.com/B-Con/crypto-algorithms/blob/master/sha256.c  
https://github.com/ARMmbed/mbedtls/tree/master/include/mbedtls  
https://tls.mbed.org/sha-256-source-code  
http://stackoverflow.com/questions/7196552/opencl-is-it-possible-to-invoke-another-function-from-within-a-kernel  

### Procédure
Faire des tests sur taille fixe prédéfini et alphabet simple, en python (sur CPU). 
On hash un mot et on essaye de retrouver le message original. <br> 
Passer à l'implémentation GPU.  

#### Implémentation CPU
Nous avons utilisé la fonction SHA256 de "Crypto.Hash" pour hasher un message. Notre objectif était de retrouver le message original à partir d'un hash. Pour se faire nous avons réalisé une attaque par force brute en générant des mots de même taille et en réalisant une comparaison pour chaque mot généré.

#### Implémentation GPU
A l'aide d'un code trouvé sur internet, nous avons réalisé une implémentation de la fonction de hashage dans le kernel.

Le but final étant de pouvoir executer notre fonction de hash par chaque thread disponible, afin de générer le plus grand nombre de possibilités le plus rapidement possible.

#### Mémoire GPU
![GPU memory architecture.png](GPU memory architecture.png "")

4 types de mémoire accessibles par unité de calcul des GPU:
<li>Mémoire globale, accessible aussi par le CPU (via buffers) **3072 Mo**</li>
<li>Mémoire constante, partie de la mémoire globale à la différence que le device n'a que le droit de lecture sur cette mémoire **64 Ko**</li>
<li>Mémoire locale, accessible seulement à partir de l'espace de travail associé **48 Ko**</li>
<li>Mémoire privée, semblable au registre d'un coeur CPU.</li>

http://www.inf.ed.ac.uk/publications/thesis/online/IM111027.pdf


####Global/Local Worksize group
https://devtalk.nvidia.com/default/topic/491298/local-global-work-group-sizes-and-memory-limit-calculations-how-to-find-out-how-much-private-mem/
https://software.intel.com/sites/landingpage/opencl/optimization-guide/Basic_Concepts.htm
http://stackoverflow.com/questions/26804153/opencl-work-group-concept

**Exemple :**
On prend comme GWS: 8 et comme LWS: 2 <br>
Nous avons donc 4 groupes de 2 Work Item: **[ (0,1), (2,3), (4,5), (6,7) ]**

get_global_id :
<li> **HORS GROUPE :** 0
<li> **DANS THREAD :** 0,1,2,3,4,5,6 ou 7 </li>

get_local_id :
<li> **HORS GROUPE :** 0,1,2,3,4,5,6 ou 7 ||  *local = tout*
<li> **DANS THREAD :** 0 ou 1 || *local = à l'intérieur d'un groupe*</li> 

####Avancement:
Nous avons réussi à faire l'implémentation CPU. <br>
L'implémentation séquentielle GPU nous pose quelques problèmes de mémoire, nous n'en aurions pas assez apparemment. <br>Nous espérons qu'avec l'implémentation parallèle nous ne recontrerons pas ces problèmes de mémoire

### Test de mémoire :
**Premier test :** on transfère une matrice 10 000 x 10 000, ce qui correspond à 400 Mo de données. Dans le noyau on affecte à chaque élèment la valeur de son numéro de ligne multiplié par son numéro de colonne. Ces calculs sont effectués en monothread.

Cette opération provoque une erreur <span style="color:red">"OUT_OF_RESSOURCES"</span> lorsqu'on l'effectue plus de deux fois d'affilée.

**Consommation : 462 Mo**

##<span style="color:green">Hypothèses :</span>
<li>modifier les valeurs plusieurs fois de suite ne fonctionne pas.
<li>la sortie s'écrit sur une nouvelle plage de données à chaque boucle, entrainant ainsi un débordement de la mémoire disponible.</li>

**Deuxième test :** on transfère une matrice 10 x 10 cette fois. On effectue la même opération un certain nombre de fois:
<li> MONOTHREAD : 3 million de fois : OK <br>Plus de 4 million de boucles : erreur <span style="color:red">"OUT_OF_RESSOURCES"</span>
<li> 4 THREADS : 750 000 boucles : OK <br>Plus de 1 million de boucles : erreur <span style="color:red">"OUT_OF_RESSOURCES"</span> </li>

**Consommation : 82 Mo**

TO DO :
<br> faire un programme simple qui n'utilise pas de mémoire mais beaucoup d'opérations. <br> 
egg : Rotation infinie des éléments d'un petit tableau.

regarder le "loop unroll" (options de compilation)

Réflechir à ce que l'on peut présenter pour la soutenance (schéma, tree...)

##<span style="color:green">Réponse :</span>
Notre variable d'itération (un k dans un for) dépassait la valeur limite pour un int32 (2**31)