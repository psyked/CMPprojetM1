from __future__ import absolute_import
from timeit import default_timer as timer
from Crypto.Hash import SHA256

import ChoixAutoDevice
import numpy as np
import pyopencl as cl

limit = 10
nbthread = 1
nbblock = 1
data_in = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).astype(np.int32)
data_out = np.ones_like(data_in, np.int32)

device = ChoixAutoDevice.auto_device_selection()
context = cl.Context([device])
kernel = """
__kernel void stress_test(__global int *input, __global int *output){
    long i, j, k;
    int stop = 0;
    for(i = 0; stop < 10; i++){
        k = input[0];
        for(j = 0; j < 10; j++){
            input[j] = input[j+1];
        }
        input[9] = k;
        if(i == 214748363){
            i = 0;
            stop += 1;
        }

    }
    printf("%ld \\n", i);
    for(int n = 0; n < 10; n++){
        output[n] = input[n];
    }
    output[0] = i;
}
"""

queue = cl.CommandQueue(context)
mem_flags = cl.mem_flags
program = cl.Program(context, kernel).build()

start = timer()
input_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=data_in)
output_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, data_out.nbytes)
completeEvent = program.stress_test(queue, (nbthread, nbthread), (nbblock, nbblock), input_buf, output_buf)
cl.enqueue_copy(queue, data_out, output_buf).wait()
end = timer()

print data_out
print end - start



