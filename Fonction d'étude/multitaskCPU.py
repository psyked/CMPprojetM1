import multiprocessing
from random import randrange
import numpy
from scipy.ndimage import generic_filter
from statsmodels.distributions.empirical_distribution import ECDF
import time


def stat_function(x, nodata):
    center = x[len(x) / 2]
    if center == nodata:
        return nodata
    else:
        xx = [value for value in x if value != nodata]
        ecdf = ECDF(xx)
        percentile = (1 - ecdf(center)) * 100
        return percentile


def worker(procnumber, table, function, size, mode, extra_arguments, return_dic):
    # used to cast values of the table into doubles that'll force scipy to return a table of doubles
    # it'll return a table of integer otherwise
    # table = numpy.array(table, dtype=numpy.double)
    stats_downsamp = generic_filter(table,
                                    function=function,
                                    size=size,
                                    mode=mode,
                                    extra_arguments=extra_arguments)
    return_dic[procnumber] = stats_downsamp


def divide_and_rule(table, function, size, mode, extra_arguments, split=(2, 2)):
    """split the 2D table into split[0]*split[1] tables which will be compute on a different thread (physic)
     PS : the split/reassembling code only works for (2,2), (3,3), (4,4) etc. and isn't at all optimised,
            don't use it outside tests"""
    splits = []
    for i in range(0, split[0]):
        data = table[i * len(table) / split[0]:((i + 1) * len(table) / split[0])]
        for j in range(0, split[1]):
            array = []
            for k in range(0, len(data)):
                array.append(data[k][j * len(table) / split[1]:((j + 1) * len(table) / split[1])])
            splits.append(array)
    # using a manager to share a dictionary between processes
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    myqueue = []
    for i in range(0, len(splits)):
        # create a pool of process and start them
        myqueue.append(multiprocessing.Process(target=worker,
                                               args=(i, splits[i], function, size, mode, extra_arguments, return_dict)))
        myqueue[i].start()
    for i in range(0, len(splits)): myqueue[i].join()  # barrier where all processes will wait the end of all the others

    # now let's rebuild the table from the splited one returned
    return_array = []
    for i in range(0, split[0]):
        for k in range(0, len(table) / split[0]):
            line = []
            for j in range(0, split[1]):
                for l in range(0, len(return_dict[j][k])):
                    line.append(return_dict[i * split[1] + j][k][l])
            return_array.append(line)
    return return_array


if __name__ == '__main__':
    # table of test filled with random values
    table = []
    for i in range(0, 200):
        line = []
        for j in range(0, 200):
            line.append(randrange(0, 25))
        table.append(line)
    # used to cast values of the table into doubles that'll force scipy to return a table of doubles
    # otherwise, it'll return a table of integer
    table = numpy.array(table, dtype=numpy.double)
    # params
    mode = 'reflect'
    window_size = (41, 41)
    mtime = time.time()
    split = (2, 2)  # define the tables ta create. Ex : (2,3) will cut the table in half vertically then in 3 parts horizontally
    # end params

    # just use the function like the scipy's one, just add the split parameter
    returned_data = divide_and_rule(table,
                                    function=stat_function,
                                    size=window_size,
                                    mode=mode,
                                    extra_arguments=(-1,),
                                    split=split)
    print "after %ssec on %s treads, we get :" % (time.time() - mtime, split[0] * split[1])
    print numpy.array(returned_data)  # reshape table for printing, it'll be on one line otherwise

    # same calculations with the original programme
    mtime = time.time()
    stats_downsamp = generic_filter(table,
                                    function=stat_function,
                                    size=window_size,
                                    mode=mode,
                                    extra_arguments=(-1,))
    print "After %s seconds with the initial function, we get :" % (time.time() - mtime)
    print stats_downsamp
