import pyopencl as cl
from random import randrange
import numpy
import time


def pyopencl_percentile_filter(array, window, nodata=-1, mode="reflect", constant=0, nbthreads=0):
    # use if you want to enable the compiler output
    # import os
    # os.environ['PYOPENCL_COMPILER_OUTPUT'] = '1'

    # chose the platform and device to compile and run on
    platform = cl.get_platforms()[0]
    device = platform.get_devices()[0]

    # translate modes into constant integer for the C kernel
    if mode == "reflect": mode = 0
    elif mode == "wrap": mode = 1
    else: mode = 2

    context = cl.Context([device])
    # get the kernel to build
    file = open("kernel.c", "r")
    myprgm = file.read()
    file.close()
    # we'll use one thread per sample or the number given
    nb_threads = len(array)*len(array[0])if(nbthreads <= 0)else nbthreads
    # set defines and enable the extensions openCL
    define = """
        #define X_WINDOW """ + str(window[0]) + """
        #define Y_WINDOW """ + str(window[1]) + """
        #define MODE """ + str(mode) + """
        #define CONSTANT """ + str(constant) + """
        #define NODATA """ + str(nodata) + """
        #define NBTHREADS """ + str(nb_threads) + """
        #define MATRIX_SIZE_X """ + str(len(array)) + """
        #define MATRIX_SIZE_Y """ + str(len(array[0])) + """\n\n"""
    # add the 'constants' to the C kernel
    myprgm = define + myprgm
    # build the kernel with the defines for the targeted device
    program = cl.Program(context, myprgm).build()
    queue = cl.CommandQueue(context)

    # create the empty table to fill with the kernel
    array_returned = []
    for i in range(0, len(array)):
        line = []
        for j in range(0, len(array[0])):
            line.append(0)
        array_returned.append(line)
    array_returned = numpy.array(array_returned)

    # bufferise global variables into the GPU
    mem_flags = cl.mem_flags
    # the data to be treated
    array = numpy.array(array, dtype=numpy.double)
    arrayB = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=array)
    # the buffer that'll be returned with the percentiles
    array_returned = numpy.array(array_returned, dtype=numpy.double)
    array_returnedB = cl.Buffer(context, mem_flags.WRITE_ONLY, array_returned.nbytes)
    # we chose which kernel to use
    kernel = program.ECDF_and_percentile
    # kernel.set_scalar_arg_dtypes([None, None,None, int]) # used to pass direct value into kernel
    kernel(
        queue,  # the queue where the prgm will be executed
        (nb_threads, 1),  # the number of treads per work group
        None,
        arrayB,  # buffers to access from openCL kernel
        array_returnedB
    )
    # wait for the end of the kernel executed in the queue then copy the buffer array_returnedB into array_returned
    cl.enqueue_copy(queue, array_returned, array_returnedB)
    return array_returned


if __name__ == "__main__":
    #### parameters
    NBTHREADS = 1800
    XWINDOW = 11
    YWINDOW = 11
    MATRIXSIZE = [1000, 1000]
    MODE = "reflect"  # 'reflect' 'constant' or 'wrap'
    NODATA = -1
    CONSTANT = 0 # used if MODE == 'constant'
    #### end parameters
    array = []
    for i in range(0, MATRIXSIZE[0]):
        line = []
        for j in range(0, MATRIXSIZE[1]):
            line.append(j+10*i)
        array.append(line)
    print "kernel : "
    m_time = time.time()
    returned = pyopencl_percentile_filter(array, (XWINDOW, YWINDOW), NODATA, MODE, CONSTANT, NBTHREADS)
    print "sortie kernel (apres %s sec) : " % (time.time() - m_time)
    print(returned)
