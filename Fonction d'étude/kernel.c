void swap(double table[], int a, int b)
{
    double buff = table[a];
    table[a] = table[b];
    table[b] = buff;
}
int partition(double *arr, int l, int h)
{
    double x = arr[h];
    int i = (l - 1);

    for (int j = l; j <= h- 1; j++)
    {
        if (arr[j] <= x)
        {
            i++;
            swap(arr, i, j);
        }
    }
    swap(arr, i+1, h);
    return (i + 1);
}
void quickSort(double *arr, int l, int h)
{
    double stack[X_WINDOW * Y_WINDOW];

    int top = -1;

    stack[ ++top ] = l;
    stack[ ++top ] = h;

    while ( top >= 0 )
    {
        h = stack[ top-- ];
        l = stack[ top-- ];

        int p = partition( arr, l, h );

        if ( p-1 > l )
        {
            stack[ ++top ] = l;
            stack[ ++top ] = p - 1;
        }

        if ( p+1 < h )
        {
            stack[ ++top ] = p + 1;
            stack[ ++top ] = h;
        }
    }
}

double ECDF(double *data, int size, double dataValue)
// compute the ECDF from the sorted table 'data' and return the value for the wanted data
{
    for(int i = 0; i < size; i++)
    {
        if(dataValue == data[i])
        {
            while(data[i] == dataValue && i < size) i++;
            return (double)i/(double)(size);
        }
    }
    return 1;
}

double computePercentile(double *data, int t)
{
    double center = data[(int)(X_WINDOW * Y_WINDOW / 2)];
    //for(int i = 0; i < X_WINDOW*Y_WINDOW; i++) {if(i == (int)(X_WINDOW * Y_WINDOW / 2)) {printf("\n");} printf("%lf ", data[i]);}

    if(center == NODATA) return NODATA;

    int sizeWindow = X_WINDOW * Y_WINDOW;
    quickSort(data, 0, sizeWindow-1);
    double clearedData[X_WINDOW * Y_WINDOW];
    int sizeClearedData = 0;
    for(int i = 0; i < sizeWindow; i++)
    { // create a new table with only clear data
        if(data[i] != NODATA) clearedData[sizeClearedData++] = data[i];
    }
    double value = ECDF(data, sizeClearedData, center);

    double percentile = (1 - value) * 100;
    //printf("%lf : %lf\n", center, percentile);
    return (1 - value) * 100;
}

void getSample(__global double *matrix, double *sample, int Xmin, int Ymin, int Xmax, int Ymax)
{ // set the sample from the matrix with the given window
    int iterator = 0;
    int i, j;
    for(int I = Ymin; I <= Ymax; I++)
    {
        i = I;
        if(i < 0)
        {
            switch(MODE)
            {
                case 0: i = - i - 1; break; // reflect
                case 1: i = MATRIX_SIZE_Y - i; break; // wrap
                case 2: i = -1; // constant
            }
        }
        if(i >= MATRIX_SIZE_Y)
         {
            switch(MODE)
            {
                case 0: i = MATRIX_SIZE_Y - (i - MATRIX_SIZE_Y) - 1; break;
                case 1: i -= MATRIX_SIZE_Y; break;
                case 2: i = -1;
            }
        }
        for(int J = Xmin; J <= Xmax; J++)
        {
            j = J;
            if(j < 0)
             {
                switch(MODE)
                {
                    case 0: j =  -j -1; break;
                    case 1: j = MATRIX_SIZE_X - j; break;
                    case 2: j = -1;
                }
             }
            if(j >= MATRIX_SIZE_X)
             {
                switch(MODE)
                {
                    case 0: j = MATRIX_SIZE_X - (j - MATRIX_SIZE_X) - 1; break;
                    case 1: j -= MATRIX_SIZE_X; break;
                    case 2: j = -1;
                }
            }
            //printf("%d:(%d,%d).(%d,%d) ", iterator, i, j, I, J);
            if(j < 0 ) printf("inf %d\n", j);
            if(j > MATRIX_SIZE_X) printf("sup %d\n", j);

            if(i < 0 ) printf("i inf %d\n",i);
            if(i > MATRIX_SIZE_Y) printf("i sup %d\n", i);

            if(i < 0 || j < 0) sample[iterator++] = CONSTANT;
            else sample[iterator++] = matrix[i * MATRIX_SIZE_X + j];
        }
    }
}

__kernel void ECDF_and_percentile(__global double *matrix, __global double *result)
{
    // get the id of the thread
    int gid = get_global_id(0);

    // define the number of loops (element) each thread has to process
    int loops = (int)(MATRIX_SIZE_X * MATRIX_SIZE_Y / NBTHREADS) + (((MATRIX_SIZE_X * MATRIX_SIZE_Y) % NBTHREADS == 0)? 0 : 1);
    double sample[X_WINDOW * Y_WINDOW];
    for(int i = 0; i < loops; i++)
    {
        int id = gid + i * NBTHREADS;
        if(id >= MATRIX_SIZE_X * MATRIX_SIZE_Y) continue;
        //if(id == 0) printf("%d  %d  %d  %d\n", X_WINDOW, Y_WINDOW, MATRIX_SIZE_X, MATRIX_SIZE_Y);
        // determinate the window to use with this thread
        int Xmin = (id % MATRIX_SIZE_X) - X_WINDOW/2;
        int Ymin = (id / MATRIX_SIZE_X) - Y_WINDOW/2;
        int Xmax = (id % MATRIX_SIZE_X) + X_WINDOW/2 - ((X_WINDOW%2 == 0)? 1 : 0);
        int Ymax = (id / MATRIX_SIZE_X) + Y_WINDOW/2 - ((Y_WINDOW%2 == 0)? 1 : 0);
        //if(id == 9) printf("%d : (%d, %d)-(%d, %d)\n",id,Xmin,Ymin,Xmax,Ymax);
        // copy the sample
        getSample(matrix, sample, Xmin, Ymin, Xmax, Ymax);
        // get the result
        result[id] = computePercentile(sample, id);
    }
}
