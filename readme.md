# Projet d'étude M1 S1 ISEN sur le calcul massivement parallélisé par GPU

## Description

Nous étudions les différences entre les architectures GPU et CPU et les différentes librairies pour coder sur GPU.<br/>
Nous avons donc tout d'abord effectué des benchmarks sous c++11, gnumpy, pyopenCL, Cuda, cudaMath, pyViennaCL et gnumpy. Voir dossier étude préliminaire <br/>
Puis choisit pyopenCL pour implémenter des projets sous GPU : cassage de mot de passe haché par SHA256 et TSP (approche exhaustive et génétique). Voir dossier exemples OpenCL <br/>
Enfin, nous avons parallélisé sous CPU puis GPU un script python donné. Voir dossier Fonction d'étude.

## Contributeurs

- Edouard François
- Florian Desrousseaux
- Pierre Barre 
- Romain Vanmarcke