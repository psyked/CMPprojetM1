#include<iostream>
#include<thread>
#include<stdlib.h>
#include<vector>
#include "RealTimeTimer.cpp"
#include "Matrix.cpp"
#include "Stats.cpp"
#include<omp.h>


using namespace std;
//todo : 
// how to implemente/code algos faster on opencl/cuda 
int main(int argc, char** argv)
{
	int arg1 = 1024 * 2;
	int arg2 = 1024 * 2;
	int arg3 = 0;
	int arg4 = 5;
	int arg5 = 4;
	int arg6 = 0;

	if (argc >= 3)
	{ //heigh, width of the matrix to build
		arg1 = atoi(argv[1]);
		arg2 = atoi(argv[2]);
	}
	if (argc >= 4)
	{//addition or square or other operation to code later
		arg3 = atoi(argv[3]);
	}
	if (argc >= 5)
	{// repete X time the calculations
		arg4 = atoi(argv[4]);
	}
	if (argc >= 6)
	{//number of thread to launch
		arg5 = atoi(argv[5]);
	}
	if (argc >= 7)
	{//int or float
		arg6 = atoi(argv[6]);
	}

	srand(time(NULL));

	std::cout << "donnee dans l'ordre int64, double, float puis int" << endl;
	for (arg1 = 128; arg1 <= 1024 * 2; arg1 *= 2)
	{
		arg2 = arg1;
		std::cout << "taille matrice NxN : " << arg1 << endl;

		for (arg3 = 0; arg3 < 1; arg3++)
		{
			for (arg5 = 4; arg5 <= 4; arg5++)
			{
				for (arg6 = 0; arg6 <= 3; arg6++)
				{
					/*cout << "debut test matrice " << arg1 << "x" << arg2 << " random " << "avec " << arg5
						<< " threads (une operation supplementaire par thread) repete "
						<< arg4 << " fois :";*/

					RealTimeTimer mainTimer = RealTimeTimer();
					RealTimeTimer timer = RealTimeTimer();
					mainTimer.start();
					Stats<int> stats = Stats<int>();
					for (int i = 0; i < arg4; i++)
					{
						omp_set_num_threads(arg5);
						switch (arg6)
						{
						case 0:
						{
							//std::cout << " int64 ";
							Matrix<int64_t> m1 = Matrix<int64_t>(arg1, arg2, true);
							Matrix<int64_t> m2;
							timer.start();
							switch (arg3)
							{
							case 1:
								//cout << "addition" << endl;
								m2 = m1 + m1 + m1 + m1 + m1 + m1;
								break;
							case 2:
								//cout << "multiplication point a point" << endl;
								m2 = m1.point(m1).point(m1).point(m1).point(m1);
								break;
							default:
								//cout << "au carre" << endl;
								m2 = m1*m1;
							}
							break;
						}
						case 1:
						{
							//std::cout << " double ";
							Matrix<double> m1 = Matrix<double>(arg1, arg2, true);
							Matrix<double> m2 = Matrix<double>(arg1, arg2, true);
							Matrix<double> m3;
							timer.start();
							switch (arg3)
							{
							case 1:
								//cout << "addition" << endl;
								m2 = m1 + m1;
								break;
							case 2:
								//cout << "multiplication point a point" << endl;
								m2 = m1.point(m1);
								break;
							default:
								m3 = m1*m2;
								//	cout << "au carre" << endl;								
							}
							break;
						}
						case 2:
						{
							//std::cout << " float ";
							Matrix<float> m1 = Matrix<float>(arg1, arg2, true);
							Matrix<float> m2 = Matrix<float>(arg1, arg2, true);
							Matrix<float> m3;
							timer.start();
							switch (arg3)
							{
							case 1:
								//cout << "addition" << endl;
								m2 = m1 + m1;
								break;
							case 2:
								//cout << "multiplication point a point" << endl;
								m2 = m1.point(m1);
								break;
							default:
								m3 = m1*m2;
								//	cout << "au carre" << endl;	
							}
							break;
						}
						default:
						{
							//std::cout << " int ";
							Matrix<int> m1 = Matrix<int>(arg1, arg2, true);
							Matrix<int> m2 = Matrix<int>(arg1, arg2, true);
							Matrix<int> m3;
							timer.start();
							switch (arg3)
							{
							case 1:
								//cout << "addition" << endl;
								m2 = m1 + m1;
								break;
							case 2:
								//cout << "multiplication point a point" << endl;
								m2 = m1.point(m1);
								break;
							default:
								m3 = m1*m2;
								//	cout << "au carre" << endl;
							}
							break;
						}
						}
						timer.stop();
						stats.addElem(timer.duration());

					}
					mainTimer.stop();
					//cout << "fin test apres " << mainTimer.duration() << "microsec" << endl;
					std::cout << stats.getAverage() << endl;
					/*if (arg4 > 1)
					{
						cout << "Stats : moyenne :" << stats.getAverage() << ", quartiles : " << stats.getQuartiles()[0] << " "
							<< stats.getQuartiles()[1] << " " << stats.getQuartiles()[2] << ", ecart-type : " << sqrt(stats.getVariance()) << endl;
					}*/
				}
			}
		}
	}
	/*
	Matrix<int> m1 = Matrix<int>(4,4,true);
	m1.print();
	cout << endl;
	m1 = m1*m1;
	m1.print();
	cout << endl;*/
	char a;
	std::cin >> a;

	return 0;
}

