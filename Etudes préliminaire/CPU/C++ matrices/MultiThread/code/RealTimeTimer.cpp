
#include<time.h>
#include<ctime>
#include<thread>

class RealTimeTimer
{
private:
	std::chrono::system_clock::time_point m_start;
	std::chrono::system_clock::time_point m_stop;
public:
	void start()
	{
		m_start = std::chrono::system_clock::now();
	}

	void stop()
	{
		m_stop = std::chrono::system_clock::now();
	}

	long duration()
	{
		auto elapsed = m_stop - m_start;
		long ms = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();

		return ms;
	}
};



