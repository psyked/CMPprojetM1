#include<vector>
#include<iostream>

using namespace std;

template<class T>
class Matrix
{
	vector<vector<T>> m_matrix;
	int m_heigh;
	int m_width;

public:
	vector<vector<T> > getNewMatrix(int x, int y, bool random)
	{
		//std::cout << " vars :" << x << y << random << std::endl;
		vector<vector<T> > matrix = vector<vector<T> >(x);
		
		for (int i = 0; i < x; i++)
		{
			vector<T> buff = vector<T>(y);
			for (int j = 0; j < y; j++)
			{
				if (random)
				{
					buff[j] = (rand() % 200)*0.5211;//(T)((rand() % 200)*0.5211);
				}
				else
				{
					buff[j] = 0;
				}
			}
			matrix[i] = buff;
		}
		//std::cout << "size : " << matrix.size() << std::endl;
		return matrix;
	}

	Matrix square()
	{
		Matrix<T> m = Matrix<T>();
		vector<vector<T> > mM = getMatrix();
		m.setMatrix(mM);
		return m * m;
	}
	void print()
	{
		for (int i = 0; i < m_heigh; i++)
		{
			for (int j = 0; j < m_width; j++)
			{
				cout << m_matrix[i][j] << " ";
			}
			cout << endl;
		}
	}
	vector<vector<T> > getMatrix()
	{
		return m_matrix;
	}
	Matrix point(Matrix& m)
	{
		Matrix matrix = Matrix<T>(m_heigh, m_width, false);
		vector<vector<T> > matr = matrix.getMatrix();
		vector<vector<T> > mM = m.getMatrix();
#pragma omp parallel 
		for (int i = omp_get_thread_num(); i < m_heigh; i += omp_get_num_threads()) 
		{
			for (int k = 0; k < m_width; k++)
			{
				matr[i][k] += (m_matrix[i][k] * mM[i][k]);
			}
		}
#pragma omp barrier
		matrix.setMatrix(matr);
		return matrix;
	}
	Matrix operator+(Matrix& m)
	{
		const int *size = m.getXY();
		if (size[0] != m_heigh || size[1] != m_width)
		{
			return m;
		}
		vector<vector<T> > matr = m.getMatrix();
#pragma omp parallel 
		for (int i = omp_get_thread_num(); i < size[0]; i += omp_get_num_threads())
		{
			for (int j = 0; j < size[1]; j++)
			{
				matr[i][j] += m_matrix[i][j];
			}
		}
#pragma omp barrier

		m.setMatrix(matr);
		return m;
	}
	void Transpose(vector<vector<T> >& m)
	{
		for (int i = 0; i < m.size(); i++) {
			for (int j = i + 1; j < m[0].size(); j++) {
				std::swap(m[i][j], m[j][i]);
			}
		}
	}
	vector<vector<T> > multiplicationByTransposition(vector<vector<T> > &m1, vector<vector<T> >& m2)
	{
		Transpose(m2);
		vector<vector<T> > result = getNewMatrix(m1.size(), m1[0].size(), false);
#pragma omp parallel 
		for (int i = omp_get_thread_num(); i < m1[0].size(); i += omp_get_num_threads()) 
		{
			for (int j = 0; j < m1.size(); j++) 
			{
				multiplicationOneElementTranspose(i ,j, m1, m2, result);
			}
		}
		return result;
	}

	void multiplicationBlockPaeno(int x, int y, vector<vector<T> > &m1, vector<vector<T> >& m2, vector<vector<T> >& result)
	{
		multiplicationOneElementTranspose(x, y, m1, m2, result);
		multiplicationOneElementTranspose(x, ++y, m1, m2, result);
		multiplicationOneElementTranspose(++x, y, m1, m2, result);
		multiplicationOneElementTranspose(x, --y, m1, m2, result);
		multiplicationOneElementTranspose(++x, y, m1, m2, result);
		multiplicationOneElementTranspose(x, ++y, m1, m2, result);
		multiplicationOneElementTranspose(x, ++y, m1, m2, result);
		multiplicationOneElementTranspose(--x, y, m1, m2, result);
		multiplicationOneElementTranspose(--x, y, m1, m2, result);
	}

	void multiplicationBlockNaive(int x, int y, int size, vector<vector<T> > &m1, vector<vector<T> >& m2, vector<vector<T> >& result)
	{
		for (int i = x; i < x+size; i++)
		{
			for (int j = y; j < y+size; j++)
			{
				multiplicationOneElementTranspose(i, j, m1, m2, result);
			}
		}
	}
	vector<vector<T> > multiplicationBlock(vector<vector<T> > &m1, vector<vector<T> >& m2)
	{
		Transpose(m2);
		//int cache = 6 //MB
		int size = 8; //block size x size
		//int size = 3;// if paeno
		vector<vector<T> > result = getNewMatrix(m1.size(), m1[0].size(), false);
#pragma omp parallel 
		{
			int i, j;
			for (i = omp_get_thread_num()*size; i < (m1[0].size()-size); i += size*omp_get_num_threads())
			{//parcours les x de 4 en 4
				for (j = 0; j < (m1.size()-size); j += size)
				{//parcours les y de 4 en 4
						multiplicationBlockNaive(i, j, size, m1, m2, result);
						//multiplicationBlockPaeno(i, j, m1, m2, result);				
				}
				if (j < m1.size())
				{//on ne peut plus decoupe en bloc (mais il reste des elements non traites)
					for (j; j < m1.size(); j++)
					{
						int I = i;
						for (I; I < i + size; I++)
						{
							multiplicationOneElementTranspose(I, j, m1, m2, result);
						}
					}
				}
			}
			if (i < m1.size())
			{//on ne peut plus decouper de bloc size x size
				for (i; i < m1.size(); i++)
				{
					for (int j = 0; j < m1[0].size(); j++)
					{
#pragma omp critical
						{
							multiplicationOneElementTranspose(i, j, m1, m2, result);
						}
					}
				}
			}
		}
		return result;
	}

	void multiplicationOneElementTranspose(int x, int y, vector<vector<T> >& m1, vector<vector<T> >& m2, vector<vector<T> >& result)
	{
		T r = 0;
		for (int k = 0; k < m1.size(); k++)
		{
			r += m1[x][k] * m2[y][k];
		}
		result[x][y] = r;
	}
	void multiplicationOneElement(int x, int y, vector<vector<T> >& m1, vector<vector<T> >& m2, vector<vector<T> >& result)
	{
		T r = 0;
		for (int k = 0; k < m1.size(); k++)
		{
			r += m1[k][y] * m2[x][k];
		}
		result[x][y] = r;
	}
	Matrix operator*(Matrix& m)
	{
		int *size = m.getXY();
		if (size[1] != m_heigh || size[0] != m_width)
		{
			return m;
		}
		//create a square matrix filled with 0s then get the array
		Matrix<T> matrix = Matrix<T>();
		/*
		vector<vector<T> > matr = getNewMatrix(m_heigh, m_heigh, false);
		vector<vector<T> > mM = m.getMatrix();
		
#pragma omp parallel 		
		{
			for (int i = omp_get_thread_num(); i < m_heigh; i += omp_get_num_threads())
			{
				for (int j = 0; j < m_heigh; j++)
				{
					for (int k = 0; k < m_width; k++)
					{
						matr[j][i] += (m_matrix[j][k] * mM[k][i]);
					}
				}
			}
#pragma omp barrier
		}*/

		//matrix.setMatrix(multiplicationByTransposition(m_matrix, m.getMatrix()));
		matrix.setMatrix(multiplicationBlock(m_matrix, m.getMatrix()));
		return matrix;
	}
	Matrix()
	{
		m_heigh = 0;
		m_width = 0;
	}
	Matrix(int x, int y)
	{
		Matrix(x, y, false);
	}
	Matrix(int x, int y, bool random)
	{
		if (x < 0 && y < 0)
		{
			Matrix();
			return;
		}
		m_heigh = x;
		m_width = y;
		m_matrix = getNewMatrix(x, y, random);
	}
	void setMatrix(vector<vector<T> > m)
	{
		try
		{
			int a = m[0][0];
		}
		catch (...)
		{
			cout << "m is null" << endl;
			return;
		}
		m_matrix = vector<vector<T> >(); 
		for (int i = 0; i < m.size(); i++)
		{
			vector<T> buff;
			for (int j = 0; j < m[i].size(); j++)
			{
				buff.push_back(m[i][j]);
			}
			m_matrix.push_back(buff);
		}
		m_heigh = (int)m.size();
		m_width = (int)m[0].size();
	}

	int* getXY()
	{
		int *sizes = new int[2];
		sizes[0] = m_heigh;
		sizes[1] = m_width;
		return sizes;
	}

};

