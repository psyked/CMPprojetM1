
#include<algorithm>
#include<vector>

template<class T>
class Stats
{
private:
	float* m_quartiles = new float[3];
	float m_average, m_variance;
	std::vector<T> m_table;
	void setQuartiles()
	{
		m_quartiles[0] = m_table[int(0.25*m_table.size())];
		m_quartiles[1] = m_table[int(0.5*m_table.size())];
		m_quartiles[2] = m_table[int(0.75*m_table.size())];
	}
	void setAverage()
	{
		float summ = 0;
		for (int i = 0; i < m_table.size(); i++)
		{
			summ += m_table[i];
		}
		m_average = summ / m_table.size();
	}
	void setVariance()
	{
		float variance = 0;
		for (int i = 0; i < m_table.size(); i++)
		{
			variance += (m_table[i] - m_average)*(m_table[i] - m_average);
		}
		m_variance = variance / m_table.size();
	}

public:
	Stats()
	{

	}
	Stats(float* table)
	{
		if (table != NULL)
		{
			m_table = std::vector<float>(table, table + sizeof table / sizeof table[0]);
		}
		std::sort(m_table.begin(), m_table.end());
		setAverage(); setQuartiles(); setVariance();
	}
	void setTable(float* table)
	{
		if (table != NULL)
		{
			m_table = std::vector<float>(table, table + sizeof table / sizeof table[0]);
		}
		std::sort(m_table.begin(), m_table.end());
		setAverage(); setQuartiles(); setVariance();
	}
	void addElem(float elem)
	{
		m_table.push_back(elem);
		std::sort(m_table.begin(), m_table.end());
		setAverage(); setQuartiles(); setVariance();
	}
	float getAverage()
	{
		return m_average;
	}
	float* getQuartiles()
	{
		return m_quartiles;
	}
	float getVariance()
	{
		return m_variance;
	}
	~Stats()
	{
	}
};

