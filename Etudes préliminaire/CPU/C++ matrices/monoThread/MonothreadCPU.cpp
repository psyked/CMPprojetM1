#include<iostream>
#include<thread>
#include<stdlib.h>
#include<vector>
#include<time.h>
#include<ctime>
#include<algorithm>

using namespace std;

class Timer
{
	std::chrono::system_clock::time_point m_start;
	std::chrono::system_clock::time_point m_stop;

public:

	void start()
	{
		m_start = std::chrono::high_resolution_clock::now();
	}

	void stop()
	{
		m_stop = std::chrono::high_resolution_clock::now();
	}

	long duration()
	{
		auto elapsed = m_stop - m_start;
		long ms = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();

		return ms;
	}
};

class Matrix
{
	vector<vector<int>> m_matrix;
	int m_heigh;
	int m_width;

public:
	Matrix();
	Matrix(int, int);
	Matrix(int, int, bool);
	int* getXY();
	vector<vector<int>> getMatrix();
	void setMatrix(vector<vector<int>>);
	Matrix operator+(Matrix&);
	Matrix operator*(Matrix&);
	void print();
	Matrix square();
};
Matrix Matrix::square()
{
	Matrix m = Matrix();
	vector<vector<int>> mM = getMatrix();
	m.setMatrix(mM);
	return m * m;
}
void Matrix::print()
{
	for (int i = 0; i < m_heigh; i++)
	{
		for (int j = 0; j < m_width; j++)
		{
			cout << m_matrix[i][j] << " ";
		}
		cout << endl;
	}
}
vector<vector<int>> Matrix::getMatrix()
{
	return m_matrix;
}
Matrix Matrix::operator+(Matrix& m)
{
	const int *size = m.getXY();
	if (size[0] != m_heigh || size[1] != m_width)
	{
		return m;
	}
	vector<vector<int>> matr = m.getMatrix();
	for (int i = 0; i < size[0]; i++)
	{
		for (int j = 0; j < size[1]; j++)
		{
			matr[i][j] += m_matrix[i][j];
		}
	}
	m.setMatrix(matr);
	return m;
}
Matrix Matrix::operator*(Matrix& m)
{
	int *size = m.getXY();
	if (size[1] != m_heigh || size[0] != m_width)
	{
		return m;
	}
	//create a square matrix filled with 0s then get the array
	vector<vector<int>> matr = Matrix(m_heigh, m_heigh, false).getMatrix();
	vector<vector<int>> mM = m.getMatrix();

	for (int i = 0; i < m_heigh; i++)
	{
		for (int j = 0; j < m_heigh; j++)
		{
			for (int k = 0; k < m_width; k++)
			{
				matr[j][i] += (m_matrix[j][k] * mM[k][i]);
			}
		}
	}
	m.setMatrix(matr);
	return m;
}
Matrix::Matrix()
{
	m_heigh = 0;
	m_width = 0;
}
Matrix::Matrix(int x, int y)
{
	Matrix(x, y, false);
}
Matrix::Matrix(int x, int y, bool random)
{
	if (x < 0 && y < 0)
	{
		Matrix();
		return;
	}
	m_matrix = vector<vector<int> >(x);
	m_heigh = x;
	m_width = y;

	for (int i = 0; i < x; i++)
	{
		vector<int> buff = vector<int>(y);
		for (int j = 0; j < y; j++)
		{
			if (random)
			{
				buff[j] = rand() % 100;
			}
			else
			{
				buff[j] = 0;
			}
		}
		m_matrix[i] = buff;
	}
}
void Matrix::setMatrix(vector<vector<int>> m)
{
	m_matrix = vector<vector<int>>();
	for (int i = 0; i < m.size(); i++)
	{
		vector<int> buff;
		for (int j = 0; j < m[i].size(); j++)
		{
			buff.push_back(m[i][j]);
		}
		m_matrix.push_back(buff);
	}
	m_heigh = (int)m.size();
	m_width = (int)m[0].size();
}

int* Matrix::getXY()
{
	int *sizes = new int[2];
	sizes[0] = m_heigh;
	sizes[1] = m_width;
	return sizes;
}

int main(int argc, char** argv)
{
	int arg1 = 250;
	int arg2 = 250;
	int arg3 = 0;
	int arg4 = 30;
	if (argc >= 3)
	{
		arg1 = atoi(argv[1]);
		arg2 = atoi(argv[2]);
	}
	if (argc >= 4)
	{
		arg3 = atoi(argv[3]);
	}
	if (argc >= 5)
	{
		arg4 = atoi(argv[4]);
	}
	Timer timer = Timer();

	srand(time(NULL));
	cout << "debut test matrice " << arg1 << "x" << arg2 << " random " << arg4 << " fois :" << endl;
	switch (arg3)
	{
	case 1:
		cout << "addition" << endl;
		break;
	default:
		cout << "au carre" << endl;
	}
	vector<int> table;
	for (int i = 0; i < arg4; i++)
	{
		timer.start();

		Matrix m1 = Matrix(arg1, arg2, true);
		Matrix m2;
		switch (arg3)
		{
		case 1:
			m2 = m1 + m1;
			break;
		default:
			m2 = m1.square();
		}

		timer.stop();
		table.push_back(timer.duration());
	}
	//stats
	sort(table.begin(),table.end());
	float* quartiles = new float[3];
	quartiles[0] = table[int(0.25*table.size())];
	quartiles[1] = table[int(0.5*table.size())];
	quartiles[2] = table[int(0.75*table.size())];
	
	float summ = 0;
	for (int i = 0; i < table.size(); i++)
	{
		summ += table[i];
	}
	float average = summ / table.size();
	
	float variance = 0;
	for (int i = 0; i < table.size(); i++)
	{
		variance += (table[i] - average)*(table[i] - average);
	}
	// fin stats
	cout << "Stats : moyenne : " << average << ", ecart-type : " << int(sqrt(variance)) << ", quartiles : " << quartiles[0] << " " << quartiles[1] << " " << quartiles[2] << " " << endl;
	char a;
	std::cin >> a;
	return 0;
}

