import logging
from timeit import Timer

import numpy as np

import sometest.cudamat as cm
from sometest import gnumpy as gp

if __name__ == '__main__':
    logging.basicConfig(format='%(message)s' , filename='example.log', level=logging.DEBUG)

    sizeList = [2**i for i in range (7 , 13 )]
    n = 30
    cm.init()
    gp._init_gpu()
    for size in sizeList:
        t_Numpy = []
        t_Cudamat = []
        t_Gnumpy = []
        for i in range(n):
            A = np.random.rand(size, size).astype(np.float32)
            B = np.random.rand(size, size).astype(np.float32)
            t_Numpy.append(Timer(lambda: np.dot(A,B)).timeit(number=1))
            t_Cudamat.append(Timer(lambda: cm.CUDAMatrix(A).dot(cm.CUDAMatrix(B))).timeit(number=1))
            t_Gnumpy.append(Timer(lambda: gp.dot(gp.garray(A),gp.garray(B))).timeit(number=1))
            print(i , size)

        logging.info("Numpy test "+"Current size = "+str(size)+" with "+str(n)+" tests")
        logging.info("Total time : " + str(sum(t_Numpy)))
        logging.info("Average time : " + str(sum(t_Numpy)/len(t_Numpy)))
        logging.info("Min time : " + str(min(t_Numpy)))
        logging.info("Max time : " + str(max(t_Numpy)))
        logging.info("Median : " + str(np.median(t_Numpy)))
        logging.info("Standard deviation : " + str(np.std(t_Numpy)))
        logging.info(" ")
        logging.info("Cudamat test "+"Current size = "+str(size)+" with "+str(n)+" tests")
        logging.info("Total time : " + str(sum(t_Cudamat)))
        logging.info("Average time : " + str(sum(t_Cudamat)/len(t_Cudamat)))
        logging.info("Min time : " + str(min(t_Cudamat)))
        logging.info("Max time : " + str(max(t_Cudamat)))
        logging.info("Median : " + str(np.median(t_Cudamat)))
        logging.info("Standard deviation : " + str(np.std(t_Cudamat)))
        logging.info(" ")
        logging.info("Gnumpy test "+"Current size = "+str(size)+" with "+str(n)+" tests")
        logging.info("Total time : " + str(sum(t_Gnumpy)))
        logging.info("Average time : " + str(sum(t_Gnumpy)/len(t_Gnumpy)))
        logging.info("Min time : " + str(min(t_Gnumpy)))
        logging.info("Max time : " + str(max(t_Gnumpy)))
        logging.info("Median : " + str(np.median(t_Gnumpy)))
        logging.info("Standard deviation : " + str(np.std(t_Gnumpy)))
        logging.info(" ")

