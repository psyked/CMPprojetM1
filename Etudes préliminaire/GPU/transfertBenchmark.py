"""Imports"""
import sys
from timeit import default_timer as timer
from datetime import datetime
import numpy as np
import pyopencl as cl
import pyviennacl as pc
import logging

logging.basicConfig(format='%(message)s', filename=datetime.now().strftime("Transferts_pyOpenCL_%Y-%m-%d_%H%M.log"), level=logging.DEBUG)

print("start")
a = np.zeros([1, 1])
b = pc.Matrix(a)

platforms = cl.get_platforms()[0]
device = platforms.get_devices()[0]
print(device)
context = cl.Context([device])
queue = cl.CommandQueue(context)
mem_flags = cl.mem_flags
program = cl.Program(context, """__kernel
    void sum(
    __global const float *a_buf,
    __global float *res_buf)
    {
        int gid = get_global_id(0);
        int gid2 = get_global_id(1);
        int wid = get_local_size(0);
        res_buf[gid + wid * gid2] = a_buf[gid + wid * gid2] + a_buf[gid + wid * gid2];
    }
    """).build()

for k in range(1, 12):
    size = 1024 * k
    a = np.random.rand(size, size).astype(np.float32)
    res = np.zeros_like(a)
    start = timer()
    # a_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=a)
    # res_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, res.nbytes)
    # program.sum(queue, res.shape, None, a_buf, res_buf)
    # cl.enqueue_copy(queue, res, res_buf)

    # b = pc.Matrix(a)
    # b.value
    end = timer()
    logging.info("Size : %s | Time : %s", size, end - start)
print("end")
