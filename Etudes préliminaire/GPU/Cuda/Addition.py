from numbapro import cuda, vectorize, guvectorize, check_cuda
from numbapro import void, uint8 , uint32, uint64, int32, int64, float32, float64, f8
import numpy as np
import logging
from timeit import default_timer as timer  # for timing

logging.basicConfig(format='%(message)s', filename='Addition_Bench.log', level=logging.DEBUG)

@cuda.jit('void(float32[:,:], float32[:,:], float32[:,:])')
def cu_add(a, b, c):
    """This kernel function will be executed by a thread."""
    tx = cuda.threadIdx.x  # unique thread ID within a block
    ty = cuda.threadIdx.y
    bx = cuda.blockIdx.x  # which block in the grid?
    by = cuda.blockIdx.y
    bw = cuda.blockDim.x  # what is the size of a block?
    bh = cuda.blockDim.y
    i = tx + bx * bw
    j = ty + by * bh

    if (i < c.shape[0]) and (j < c.shape[1]):
        c[i, j] = a[i, j] + b[i, j]
    cuda.syncthreads()

device = cuda.get_current_device()

# Size of the matrix
size = 128
count = 1
loop = 10

for k in range(7):
    n = size * 2**k
    print(n)
    # Host memory
    a = np.random.random((n, n)).astype(np.float32)
    b = np.random.random((n, n)).astype(np.float32)
    c = np.empty_like(a)

    # Set up enough threads for kernel
    threadsperblock = (32, 32)
    blockspergrid_x = (n + threadsperblock[0]) // threadsperblock[0]
    blockspergrid_y = (n + threadsperblock[1]) // threadsperblock[1]
    blockspergrid = (blockspergrid_x, blockspergrid_y)

    tabCuda = []
    tabCPU = []
    for j in range(loop):
        Start_Cuda = timer()
        # Assign equivalent storage on device
        da = cuda.to_device(a)
        db = cuda.to_device(b)

        # Assign storage on device for output
        dc = cuda.to_device(c)
        for i in range(count):
            # Launch kernel
            cu_add[blockspergrid, threadsperblock](da, db, dc)
        # Transfer output from device to host
        c = dc.copy_to_host()
        End_Cuda = timer()
        tabCuda.append(End_Cuda - Start_Cuda)

    for j in range(loop):
        Start_CPU = timer()
        for i in range(count):
            c = a + b
        End_CPU = timer()
        tabCPU.append(End_CPU - Start_CPU)

    logging.info("Size : %s | Boucles : %s | Calculs par boucle : %s", n, loop, count)
    logging.info("GPU CUDA -- min: %s | max: %s | avg: %s", min(tabCuda), max(tabCuda), sum(tabCuda) / float(len(tabCuda)))
    logging.info("CPU -- min: %s | max: %s | avg: %s", min(tabCPU), max(tabCPU), sum(tabCPU) / float(len(tabCPU)))
logging.info("")