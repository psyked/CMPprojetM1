import pyviennacl as p
import pyopencl as cl
import numpy as np
from timeit import default_timer as timer
import logging


logging.basicConfig(format='%(message)s', filename='PyViennaCL_findMax.log', level=logging.DEBUG)
inisize = 5
count = 1
loop = 1
# print(p._viennacl.opencl_support.get_current_context().current_device().full_info)
# c = np.arange(1, 26).reshape(size, size)

size = 512
workers = np.empty([4, 4])
a = np.random.rand(size, size)
res = 1
e = np.zeros([1, 1])

platforms = cl.get_platforms()[0]
device = platforms.get_devices()[0]
print(device)
context = cl.Context([device])

tabC = []
tabG = []
tabO = []

# vienna
for i in range(loop):
    startTime = timer()
    va = p.Matrix(a)
    maxiV = 0
    for x in range(size):
        for y in range(size):
            if maxiV < va[x, y].value:
                maxiV = va[x, y].value

    endTime = timer()
    tabG.append(endTime - startTime)
print(maxiV)

# cpu
for i in range(loop):
    startTime = timer()
    maxi = 0
    for x in range(size):
        for y in range(size):
            if maxi < a[x, y]:
                maxi = a[x, y]
    endTime = timer()
    tabC.append(endTime - startTime)
print(maxi)

# # open
# program = cl.Program(context, """__kernel
#     void sum(
#     __global const float *a_buf,
#     __global int res)
#     {
#         int gid = get_global_id(0);
#         int gid2 = get_global_id(1);
#         int wid = get_local_size(0);
#         int max = 0;
#         if(a_buf[gid + wid * gid2 ] > max){
#             max = a_buf[gid + wid * gid2 ];
#         }
#         res = max;
#     }
#     """).build()
# queue = cl.CommandQueue(context)
# mem_flags = cl.mem_flags
# for i in range(loop):
#     startTime = timer()
#     a_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=a)
#     # res_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, np.int32(res))
#     program.sum(queue, (1,), None, a_buf, np.int32(res))
#     # cl.enqueue_copy(queue, np.int32(res), res_buf)
#     endTime = timer()
#     tabO.append(endTime - startTime)
# print(res)
tabO.append(0)

print(tabG)
print(tabC)
print(tabO)
logging.info("Size : %s | Boucles : %s ", size, loop)
logging.info("GPU -- min: %s | max: %s | avg: %s", min(tabG), max(tabG), sum(tabG) / float(len(tabG)))
logging.info("CPU -- min: %s | max: %s | avg: %s", min(tabC), max(tabC), sum(tabC) / float(len(tabC)))
logging.info("OCL -- min: %s | max: %s | avg: %s", min(tabO), max(tabO), sum(tabO) / float(len(tabO)))

logging.info("")
