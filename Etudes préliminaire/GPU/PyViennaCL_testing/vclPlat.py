import pyviennacl as p
import numpy as np
from timeit import default_timer as timer
from datetime import datetime
import logging

logging.basicConfig(format='%(message)s', filename=datetime.now().strftime("SumBench_vcl_%Y-%m-%d_%H%M.log"), level=logging.DEBUG)


# Creer une matrice en numpy
for i in range(1, 8):
    size = 1024 * i
    a_np = np.random.rand(size, size)
    tabG = []
    tabC = []

    for k in range(10):
        T1 = timer()

# La convertir en objet pyViennaCL
        b = p.Matrix(a_np)

# Faire la multiplication de la matrice par elle meme
        d = b * b
        T2 = timer()

        tabG.append(T2 - T1)

        T3 = timer()
        c_np = np.dot(a_np, a_np)
        T4 = timer()

        tabC.append(T4 - T3)

    logging.info("GPU time for size : %s | %s", size, sum(tabG) / float(len(tabG)))
    logging.info("CPU time for size : %s | %s\n", size, sum(tabC) / float(len(tabC)))

# Recuperer et afficher la valeur
# print(d.value)

