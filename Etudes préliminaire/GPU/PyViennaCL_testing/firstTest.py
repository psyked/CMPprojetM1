import pyviennacl as p
import numpy as np
from timeit import default_timer as timer
import logging


logging.basicConfig(format='%(message)s', filename='PyViennaCL_bench.log', level=logging.DEBUG)
inisize = 4096
count = 10
loop = 2
# print(p._viennacl.opencl_support.get_current_context().current_device().full_info)
# c = np.arange(1, 26).reshape(size, size)

for k in range(1):
    size = inisize * 2**k
    a = np.random.rand(size, size)
    e = np.zeros([1, 1])
    print(e)
    tabC = []
    tabG = []
    for j in range(loop):
        b = p.Matrix(e)
        start_timeG = timer()
        b = p.Matrix(a)
        for i in range(count):
            d = b * b
        end_timeG = timer()
        tabG.append(end_timeG - start_timeG)

    for j in range(loop):
        start_timeN = timer()
        for i in range(count):
            result = np.dot(a, a)
        end_timeN = timer()
        tabC.append(end_timeN - start_timeN)

    # print(tabG)
    # print(tabC)
    logging.info("Size : %s | Boucles : %s | Calculs par boucle : %s", size, loop, count)
    logging.info("GPU -- min: %s | max: %s | avg: %s", min(tabG), max(tabG), sum(tabG) / float(len(tabG)))
    logging.info("CPU -- min: %s | max: %s | avg: %s", min(tabC), max(tabC), sum(tabC) / float(len(tabC)))
logging.info("")
