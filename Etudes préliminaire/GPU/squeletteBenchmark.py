"""Imports"""
import sys
from timeit import default_timer as timer
from datetime import datetime
import numpy as np
import pyopencl as cl
import pyviennacl as pc
import logging

"""Informations
Benchmark using 4 parameters :
 - the method to check (1 for pyOpenCl, 2 for pyCuda, 3 for Numpy, 4 for CPU
 - the operation (1 for sum, 2 for product, ...)            <-- ADD MORE
 - the matrix size
 - the number of loops
"""


"""Bench Functions"""
# pyOpenCl, case 1


def pyViennaCL(operation, size, loop):
    if operation == '1':
        #     ker = """__kernel
        #     void sum(
        #     __global const float *a_buf,
        #     __global const float *b_buf,
        #     __global float *res_buf)
        #     {
        #         int gid = get_global_id(0);
        #         int gid2 = get_global_id(1);
        #         int wid = get_local_size(0);
        #         res_buf[gid + wid * gid2] = a_buf[gid + wid * gid2 ] + b_buf[gid + wid * gid2];
        #     }
        #     """
        # else:
        #     return
        # if size % 16:
        #     print("size must be multiple of 16 !!")
        #     return
        #
        # workers = np.empty([16, 16])
        # a_np = np.random.rand(size, size).astype(np.float32)
        # b_np = np.random.rand(size, size).astype(np.float32)
        # res_np = np.empty_like(a_np)
        #
        # platforms = cl.get_platforms()[0]
        # device = platforms.get_devices()[0]
        # print(device)
        # context = cl.Context([device])
        # program = cl.Program(context, ker).build()
        # queue = cl.CommandQueue(context)
        # mf = cl.mem_flags
        # a_buf = cl.Buffer(context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a_np)
        # b_buf = cl.Buffer(context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=b_np)
        a = np.random.rand(size, size)
        b = np.random.rand(size, size)
        e = np.zeros([1, 1])
        va = pc.Matrix(e)
        vb = pc.Matrix(e)
        tab = []
        output = []
        for i in range(loop):
            startTime = timer()
            va = pc.Matrix(a)
            vb = pc.Matrix(b)
            d = va + vb
            endTime = timer()
            tab.append(endTime - startTime)
        output.append(min(tab))
        output.append(sum(tab) / float(len(tab)))
        output.append(max(tab))
        logging.info("Method: pyViennaCL, Operation: sum, Loops: %s, Size: %s", loop, size)
        logging.info("Min: %s | Avg: %s | Max: %s", output[0], output[1], output[2])

    if operation == '2':
        a = np.random.rand(size, size)
        b = np.random.rand(size, size)
        e = np.zeros([1, 1])
        va = pc.Matrix(e)
        vb = pc.Matrix(e)
        tab = []
        output = []
        for i in range(loop):
            startTime = timer()
            va = pc.Matrix(a)
            vb = pc.Matrix(b)
            d = va * vb
            endTime = timer()
            tab.append(endTime - startTime)
        output.append(min(tab))
        output.append(sum(tab) / float(len(tab)))
        output.append(max(tab))
        logging.info("Method: pyViennaCL, Operation: dotProduct, Loops: %s, Size: %s", loop, size)
        logging.info("Min: %s | Avg: %s | Max: %s", output[0], output[1], output[2])

    if operation == '3':
        a = np.random.rand(size, size)
        e = np.zeros([1, 1])
        va = pc.Matrix(e)
        tab = []
        output = []
        for i in range(loop):
            startTime = timer()
            va = pc.Matrix(a)
            maxi = 0
            for x in range(va.size1):
                for y in range(va.size2):
                    if maxi < va[x, y]:
                        maxi = va[x, y]
            endTime = timer()
            tab.append(endTime - startTime)
        output.append(min(tab))
        output.append(sum(tab) / float(len(tab)))
        output.append(max(tab))
        logging.info("Method: pyViennaCL, Operation: find max, Loops: %s, Size: %s", loop, size)
        logging.info("Min: %s | Avg: %s | Max: %s", output[0], output[1], output[2])

# pyCuda, case 2


# NumPy, case 3


# CPU standard, case 4

"""Main"""
logging.basicConfig(format='%(message)s', filename=datetime.now().strftime("Benchmark_%Y-%m-%d_%H%M.log"), level=logging.DEBUG)

size = int(sys.argv[3])
loop = int(sys.argv[4])
print("starting...")
if sys.argv[1] == '1':
    pyViennaCL(sys.argv[2], size, loop)
    print("done")
else:
    print("not yet implemented")
