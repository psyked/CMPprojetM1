from __future__ import absolute_import, print_function
import numpy as np
import pyopencl as cl
from timeit import default_timer as timer
from datetime import datetime
import logging

logging.basicConfig(format='%(message)s', filename=datetime.now().strftime("SumBench_%Y-%m-%d_%H%M.log"), level=logging.DEBUG)


# Choisir une plateforme OpenCL (Nvidia CUDA, Intel OpenCL, ...) de celles disponibles
platforms = cl.get_platforms()[0]
# Choisir un peripherique rattache a cette plateforme
device = platforms.get_devices()[0]
# Creer un contexte pour ce peripherique
context = cl.Context([device])
# Creer une file d'attente de commandes
queue = cl.CommandQueue(context)
# Creer le noyau du programme OpenCL et le compiler
program = cl.Program(context, """__kernel
        void sum(
        __global const float *a_buf,
        __global const float *b_buf,
        __global float *res_buf,
        int size)
        {
            int gid = get_global_id(0);
            int gid2 = get_global_id(1);
            res_buf[gid + size * gid2] = a_buf[gid + size * gid2 ] + b_buf[gid + size * gid2];
        };
        __kernel void sum64(
        __global const float *a_buf,
        __global const float *b_buf,
        __global float *res_buf,
        long size)
        {
            int gid = get_global_id(0);
            int gid2 = get_global_id(1);
            res_buf[gid + size * gid2] = a_buf[gid + size * gid2 ] + b_buf[gid + size * gid2];
        }
        """).build()

# Creer les variables cote CPU avec numpy


for i in range(10, 15):
    size = 1024 * i
    a_np = np.random.rand(size, size).astype(np.float32)
    b_np = np.random.rand(size, size).astype(np.float32)
    res_np = np.empty_like(a_np)
    tabG = []
    tabC = []

    # Creer des tampons OpenCL pour passer les donnees a copier et un tampon pour la sortie
    mem_flags = cl.mem_flags
    for k in range(10):
        T1 = timer()
        a_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=a_np)
        b_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=b_np)
        res_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, res_np.nbytes)

        # On execute le noyau avec les parametres indiques : worksizes globale et locale et tampons
        program.sum(queue, res_np.shape, None, a_buf, b_buf, res_buf, np.int32(size))

        # On recupere le resultat et on le copie dans une variable
        cl.enqueue_copy(queue, res_np, res_buf)
        T2 = timer()

        tabG.append(T2-T1)

        T3 = timer()
        c_np = a_np + b_np
        T4 = timer()

        tabC.append(T4-T3)

    logging.info("GPU time for size : %s | %s", size, sum(tabG) / float(len(tabG)))
    logging.info("CPU time for size : %s | %s\n", size, sum(tabC) / float(len(tabC)))


# a_np = np.random.rand(size, size).astype(np.float64)
# b_np = np.random.rand(size, size).astype(np.float64)
# res_np = np.empty_like(a_np)
#
# T5 = timer()
# a_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=a_np)
# b_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=b_np)
# res_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, res_np.nbytes)
#
# # On execute le noyau avec les parametres indiques : worksizes globale et locale et tampons
# program.sum64(queue, res_np.shape, None, a_buf, b_buf, res_buf, np.int64(size))
#
# # On recupere le resultat et on le copie dans une variable
# cl.enqueue_copy(queue, res_np, res_buf)
# T6 = timer()

# On affiche le resultat
# print(res_np)




