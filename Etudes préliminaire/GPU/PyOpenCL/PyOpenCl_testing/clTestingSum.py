from __future__ import absolute_import, print_function

import numpy as np
import pyopencl as cl
from pyopencl import array
from time import time

count = 1000
a_np = np.random.rand(100, 100)
b_np = np.random.rand(100, 100)

platforms = cl.get_platforms()[0]
device = platforms.get_devices()[0]
print(device)
context = cl.Context([device])
program = cl.Program(context, """__kernel void sum(
        __global const float *a,
        __global const float *b,
        __global float *result)
        {
          int gid = get_global_id(0);
          result[gid] = a[gid] + b[gid];
        }
        """).build()
queue = cl.CommandQueue(context)
mem_flags = cl.mem_flags
a_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=a_np)
b_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=b_np)
res_np = np.empty_like(a_np)
res_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, res_np.nbytes)

start_timeG = time()
for i in range(count):
    program.sum(queue, res_np.shape, None, a_buf, b_buf, res_buf)
    cl.enqueue_copy(queue, res_np, res_buf)
end_timeG = time()
print(end_timeG - start_timeG)

# print(res_np)

start_timeC = time()
for i in range(count):
    result = a_np + b_np
    end_timeC = time()
print(end_timeC - start_timeC)

# print(result)



