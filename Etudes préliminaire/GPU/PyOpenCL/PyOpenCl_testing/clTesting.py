from __future__ import absolute_import, print_function

import numpy as np
import pyopencl as cl
from pyopencl import array
from time import time, clock


vector = np.zeros((1, 1), cl.array.vec.float4)
matrix = np.zeros((1, 4), cl.array.vec.float4)
matrix[0, 0] = (1, 2, 4, 8)
matrix[0, 1] = (16, 32, 64, 128)
matrix[0, 2] = (3, 6, 9, 12)
matrix[0, 3] = (5, 10, 15, 25)
vector[0, 0] = (1, 2, 4, 8)

vectorC = np.array([1, 2, 4, 8])
matrixC = np.array([[1, 2, 4, 8], [16, 32, 64, 128], [3, 6, 9, 12], [5, 10, 15, 25]])


# a_np = np.random.rand(50000).astype(np.float32)

start_timeG = time()
platforms = cl.get_platforms()[0]
device = platforms.get_devices()[0]
print(device)
context = cl.Context([device])
program = cl.Program(context, """__kernel void matrix_dot_vector(__global const float4 *matrix,
        __global const float4 *vector, __global float *result)
        {
          int gid = get_global_id(0);
          result[gid] = dot(matrix[gid], vector[0]);
        }
        """).build()
queue = cl.CommandQueue(context)
mem_flags = cl.mem_flags
matrix_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=matrix)
vector_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=vector)
matrix_dot_vector = np.zeros(4, np.float32)
destination_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, matrix_dot_vector.nbytes)

program.matrix_dot_vector(queue, matrix_dot_vector.shape, None, matrix_buf, vector_buf, destination_buf)

cl.enqueue_copy(queue, matrix_dot_vector, destination_buf)
end_timeG = time()
print(end_timeG - start_timeG)

print(matrix_dot_vector)

start_timeC = time()
clock()
result = np.dot(matrixC, matrixC)
end_timeC = time()
print (clock())

print(end_timeC - start_timeC)

print(result)



