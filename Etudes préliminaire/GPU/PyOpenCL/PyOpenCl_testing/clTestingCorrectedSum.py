from __future__ import absolute_import, print_function

import numpy as np
import pyopencl as cl
from time import time

size = 2048
workers = np.empty([32, 32])
count = 30
a_np = np.random.rand(size, size).astype(np.float32)
b_np = np.random.rand(size, size).astype(np.float32)
res_np = np.empty_like(a_np)

platforms = cl.get_platforms()[0]
device = platforms.get_devices()[0]
print(device)
context = cl.Context([device])
program = cl.Program(context, """__kernel
        void sum(
        __global const float *a_buf,
        __global const float *b_buf,
        __global float *res_buf)
        {
            int gid = get_global_id(0);
            int gid2 = get_global_id(1);
            int wid = get_local_size(0);
            res_buf[gid + wid * gid2] = a_buf[gid + wid * gid2 ] + b_buf[gid + wid * gid2];
        }
        """).build()
queue = cl.CommandQueue(context)
mem_flags = cl.mem_flags

a_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=a_np)
b_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=b_np)
res_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, res_np.nbytes)

for i in range(5):  # Warming up
    program.sum(queue, res_np.shape, workers.shape, a_buf, b_buf, res_buf)
    cl.enqueue_copy(queue, res_np, res_buf)

start_timeG = time()
a_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=a_np)
b_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=b_np)
res_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, res_np.nbytes)
for i in range(count):
    program.sum(queue, res_np.shape, workers.shape, a_buf, b_buf, res_buf)
    cl.enqueue_copy(queue, res_np, res_buf)
end_timeG = time()
print(end_timeG - start_timeG)


# start_timeC = time()
#
# for i in range(count):
#     result = a_np + b_np
#
# end_timeC = time()
# print(end_timeC - start_timeC)

start_timeN = time()

for i in range(count):
    result = np.add(a_np, b_np)

end_timeN = time()
print(end_timeN - start_timeN)

print(device.global_mem_size)

