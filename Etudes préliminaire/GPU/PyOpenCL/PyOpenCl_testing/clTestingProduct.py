"""Imports"""
import sys
from timeit import default_timer as timer
from datetime import datetime
import numpy as np
import pyopencl as cl
import pyviennacl as pc
import logging

logging.basicConfig(format='%(message)s', filename=datetime.now().strftime("pyOpenCL_product_%Y-%m-%d_%H%M.log"),
                    level=logging.DEBUG)

print("start")
a = np.zeros([1, 1])
v = pc.Matrix(a)  # starting the GPU ~~ warm up
block_size = 16
loop = 10
count = 1

platforms = cl.get_platforms()[0]
device = platforms.get_devices()[0]
print(device)
context = cl.Context([device])
queue = cl.CommandQueue(context)
mem_flags = cl.mem_flags
program = cl.Program(context,
                     """
    __kernel void squarring(__global const float* A,
                            __global float* RES,
                            int size
                            int bsize) {
        float subRes = 0.0f;
            // 2D Thread ID
        int tx = get_local_id(0);
        int ty = get_local_id(1);
           // Block index
        int bx = get_group_id(0);
        int by = get_group_id(1);
            // Setting the block processing loop limits
        int aB = size * bsize * by;
        int aE = aB + size - 1;
        int bB = bsize * bx;

        // Loop over all the sub-matrices of A and B
        // required to compute the block sub-matrix
        for (int a = aB, b = bx * bsize; a <= aE ; a += bsize, b += bsize * size)
        {
            // Declaration of the local sub-matrix of A and B
            __local float As[bsize][bsize];
            __local float Bs[bsize][bsize];

            // Load the matrices, each thread process one value
            As[ty][tx] = A[a + size * ty + tx];
            Bs[ty][tx] = A[b + size * ty + tx];

            // Synchronize to make sure the matrices are loaded
            barrier(CLK_LOCAL_MEM_FENCE);

            // Multiply the two matrices, each thread computes one element
            for (int k = 0; k < bsize; ++k)
                subRes += As[ty][k] * Bs[k][tx];

            // Synchronize to make sure that the preceding
            // computation is done before loading two new
            // sub-matrices of A and B in the next iteration
            barrier(CLK_LOCAL_MEM_FENCE);
        }

        // Write the block sub-matrix to device memory;
        // each thread writes one element
        int c = size * bsize * by + bsize * bx;     // locate the proceeded block
        RES[c + size * ty + tx] = subRes;           // fill the block
    }
    """).build()

for k in range(1):
    size = 6144 * (2**k)
    a = np.random.rand(size, size).astype(np.float32)
    res = np.zeros_like(a)
    tabO = []
    tabC = []
    tabV = []
    for i in range(loop):

        startO = timer()
        a_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=a)
        res_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, res.nbytes)
        for j in range(count):
            program.squarring(queue, res.shape, (block_size, block_size), a_buf, res_buf, np.int32(size), np.int32(block_size))
            cl.enqueue_copy(queue, res, res_buf)
        endO = timer()

        startC = timer()
        for j in range(0):
            b = np.dot(a, a)
        endC = timer()

        startV = timer()
        v = pc.Matrix(a)
        for j in range(0):
            r = v * v
        endV = timer()

        tabO.append(endO - startO)
        tabC.append(endC - startC)
        tabV.append(endV - startV)

    logging.info("Size : %s | NumpyTime : %s | OpenTime : %s | ViennaTime : %s", size, sum(tabC) / float(len(tabC)), sum(tabO) / float(len(tabO)), sum(tabV) / float(len(tabV)))
print("end")
