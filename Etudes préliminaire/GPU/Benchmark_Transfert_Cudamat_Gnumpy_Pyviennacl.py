import logging
from timeit import Timer
import numpy as np
import gnumpy as gp
import cudamat as cm
import pyviennacl as vcl

if __name__ == '__main__':
    logging.basicConfig(format='%(message)s', filename='transfertTime.log', level=logging.DEBUG)

    sizeList = [2 ** i for i in range(7, 14)]
    n = 30
    cm.init()
    gp._init_gpu()
    for size in sizeList:
        t_Viennacl = []
        t_Cudamat = []
        t_Gnumpy = []
        for i in range(n):
            A = np.random.rand(size, size).astype(np.float32)
            t_Cudamat.append(Timer(lambda: cm.CUDAMatrix(A)).timeit(number=1))
            t_Gnumpy.append(Timer(lambda: gp.garray(A)).timeit(number=1))
            t_Viennacl.append(Timer(lambda: vcl.Matrix(A)).timeit(number=1))
            print(i, size)

        logging.info("Current size = " + str(size) + " with " + str(n) + " tests")
        logging.info("Cudamat test")
        logging.info("Total time : " + str(sum(t_Cudamat)))
        logging.info("Average time : " + str(sum(t_Cudamat) / len(t_Cudamat)))
        logging.info("Min time : " + str(min(t_Cudamat)))
        logging.info("Max time : " + str(max(t_Cudamat)))
        logging.info("Median : " + str(np.median(t_Cudamat)))
        logging.info("Standard deviation : " + str(np.std(t_Cudamat)))
        logging.info(" ")
        logging.info("Gnumpy test")
        logging.info("Total time : " + str(sum(t_Gnumpy)))
        logging.info("Average time : " + str(sum(t_Gnumpy) / len(t_Gnumpy)))
        logging.info("Min time : " + str(min(t_Gnumpy)))
        logging.info("Max time : " + str(max(t_Gnumpy)))
        logging.info("Median : " + str(np.median(t_Gnumpy)))
        logging.info("Standard deviation : " + str(np.std(t_Gnumpy)))
        logging.info(" ")
        logging.info("Viennacl test")
        logging.info("Total time : " + str(sum(t_Viennacl)))
        logging.info("Average time : " + str(sum(t_Viennacl) / len(t_Viennacl)))
        logging.info("Min time : " + str(min(t_Viennacl)))
        logging.info("Max time : " + str(max(t_Viennacl)))
        logging.info("Median : " + str(np.median(t_Viennacl)))
        logging.info("Standard deviation : " + str(np.std(t_Viennacl)))
        logging.info(" ")
