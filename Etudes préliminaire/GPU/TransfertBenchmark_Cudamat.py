"""Imports"""
from timeit import default_timer as timer
from datetime import datetime
import numpy as np
import cudamat as cm
import logging
cm.cublas_init()

logging.basicConfig(format='%(message)s', filename=datetime.now().strftime("Transferts_%Y-%m-%d_%H%M.log"), level=logging.DEBUG)

print("start")
a = np.zeros([1, 1])
b = cm.CUDAMatrix(a)
for k in range(1, 12):
    size = 128 * k
    a = np.random.rand(size, size)
    start = timer()
    b = cm.CUDAMatrix(a)
    end = timer()
    logging.info("Size : %s | Time : %s", size, end - start)
print("end")

cm.shutdown()